<?php 
// File upload folder 
$uploadDir = '/'; 
 
// Allowed file types 
$allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg'); 
 
// Default response 
$response = array( 
    'status' => 0, 
    'message' => 'Form submission failed, please try again.' 
); 
 
// If form is submitted 
// if(isset($_POST['name']) || isset($_POST['email']) || isset($_POST['file'])){ 
    // Get the submitted form data 
    // $name = 'test'; 
    // $email ='test@gmail.com';
     
    // Check whether submitted data is not empty 
    // if(!empty($name) && !empty($email)){ 
        // Validate email 
        // if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){ 
        //     $response['message'] = 'Please enter a valid email.'; 
        // }else{ 
            $uploadStatus = 1; 
             
            // Upload file 
            $uploadedFile = ''; 
            if(!empty($_FILES["file"]["name"])){ 
                // File path config 
                $fileName = basename($_FILES["file"]["name"]); 
                $targetFilePath = $uploadDir . $fileName; 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
                 
                // Allow certain file formats to upload 
                // if(in_array($fileType, $allowTypes)){ 
                    // Upload file to the server 
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){ 
                        $uploadedFile = $fileName; 
                    }else{ 
                        $uploadStatus = 0; 
                        $response['message'] = 'Sorry, there was an error uploading your file.'; 
                    } 
                // }else{ 
                //     $uploadStatus = 0; 
                //     $response['message'] = 'Sorry, only '.implode('/', $allowTypes).' files are allowed to upload.'; 
                // } 
            } 
    
        // } 
    // }else{ 
    //      $response['message'] = 'Please fill all the mandatory fields (name and email).'; 
    // } 
// } 
 
// Return response 
echo json_encode($response);