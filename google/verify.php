<?php 
session_start();
require 'vendor/autoload.php'; 
// error_reporting(E_ALL);
// ini_set('display_errors', '1');

if(isset($_POST['destroy'])){
    session_destroy();
}
if(isset($_SESSION['2income_sheet_rows'])){
    $income_sheet_rows = $_SESSION['2income_sheet_rows'];
    $master_sheet_rows = $_SESSION['2master_sheet_rows'];
    $expense_sheet_rows = $_SESSION['2expense_sheet_rows'];
    $services_sheet_rows = $_SESSION['2services_sheet_rows'];
    $customers_sheet_rows = $_SESSION['2customers_sheet_rows'];
    $assets_sheet_rows = $_SESSION['2assets_sheet_rows'];
}else{
    $client = new \Google_Client();
    $client->setApplicationName('Google Sheets API');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $path = 'credentials.json';
    $client->setAuthConfig($path);
    $service = new \Google_Service_Sheets($client);

    $spreadsheetId = '1-5TXsWZI_uJCEHU3pSEuDZsnncaJJrzuWHsm7OdZZjQ';

    $income_sheet = $service->spreadsheets_values->get($spreadsheetId, "Sale");
    $income_sheet_rows = $income_sheet->getValues();
    $_SESSION['2income_sheet_rows'] = $income_sheet_rows;


    $master_sheet = $service->spreadsheets_values->get($spreadsheetId, "Master");
    $master_sheet_rows = $master_sheet->getValues();
    $_SESSION['2master_sheet_rows'] = $master_sheet_rows;

    $expense_sheet = $service->spreadsheets_values->get($spreadsheetId, "Expense");
    $expense_sheet_rows = $expense_sheet->getValues();
    $_SESSION['2expense_sheet_rows'] = $expense_sheet_rows;

    $services_sheet = $service->spreadsheets_values->get($spreadsheetId, "Services");
    $services_sheet_rows = $services_sheet->getValues();
    $_SESSION['2services_sheet_rows'] = $services_sheet_rows;

    $customers_sheet = $service->spreadsheets_values->get($spreadsheetId, "Customers");
    $customers_sheet_rows = $customers_sheet->getValues();
    $_SESSION['2customers_sheet_rows'] = $customers_sheet_rows;

    $customers_sheet = $service->spreadsheets_values->get($spreadsheetId, "Assets");
    $assets_sheet_rows = $customers_sheet->getValues();
    $_SESSION['2assets_sheet_rows'] = $assets_sheet_rows;
}


# List Assets 
$col_no_asset = 1;
$all_assets = array();
foreach ($assets_sheet_rows as $row) {
    if(empty($row[$col_no_asset])){
        break;
    }

    $all_assets[$row[$col_no_asset]]['asset_name'] = isset($row[$col_no_asset])?$row[$col_no_asset]:'';
    $all_assets[$row[$col_no_asset]]['stock'] = 0;
    
}
// print_r($all_assets);die;


# Customers
$col_no_customer_name = 3;
$col_no_mobile = 4;
$col_no_address = 5;
$col_no_borrow_limit = 8;
$all_customers = array();
foreach ($customers_sheet_rows as $row) {
    if(empty($row[$col_no_customer_name]) && empty($row[$col_no_mobile]) && empty($row[$col_no_address])){
        break;
    }
    if(!empty($row[$col_no_customer_name])){
        $all_customers[$row[$col_no_customer_name]]['customer_name'] = isset($row[$col_no_customer_name])?$row[$col_no_customer_name]:'';
        $all_customers[$row[$col_no_customer_name]]['mobile'] = isset($row[$col_no_mobile])?$row[$col_no_mobile]:'';
        $all_customers[$row[$col_no_customer_name]]['address'] = isset($row[$col_no_address])?$row[$col_no_address]:'';
        $all_customers[$row[$col_no_customer_name]]['borrow_limit'] = isset($row[$col_no_borrow_limit])?(int)$row[$col_no_borrow_limit]:0;
    }
}
// print_r($all_customers);die;


# Services
$col_no_service_name = 1;
$col_no_charges = 2;
$col_no_commission = 4;
$col_no_actual_total = 5;
$col_no_category = 6;
$all_services = array();
$count = 0;
foreach ($services_sheet_rows as $row) { $count++;
    if(!empty($row[$col_no_service_name]) && $count>1){
        // echo $row[$col_no_service_name].'<br>';
        $all_services[$row[$col_no_service_name]]['servicename'] = isset($row[$col_no_service_name])?$row[$col_no_service_name]:'';
        $all_services[$row[$col_no_service_name]]['charges'] = isset($row[$col_no_charges])?$row[$col_no_charges]:0;
        // print_r($row);die;
        $all_services[$row[$col_no_service_name]]['commission'] = isset($row[$col_no_commission])?$row[$col_no_commission]:0;
        $all_services[$row[$col_no_service_name]]['actual_commission'] = isset($row[$col_no_actual_total])?(int)$row[$col_no_actual_total]:0 - (int)$all_services[$row[$col_no_service_name]]['charges'];
        $all_services[$row[$col_no_service_name]]['category'] = isset($row[$col_no_category])?$row[$col_no_category]:'';
    }
}
// print_r($all_services);die;


# Expenses
$begin = new DateTime('2023-12-31');
$end = new DateTime(date('Y-m-d', time()));
$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);
$date_wise_expense = array();
$col_no_date = 1;
$col_no_amount = 6;
$col_no_asset = 3;
$col_no_stock = 5; //print_r($expense_sheet_rows);die;
foreach ($expense_sheet_rows as $row) { 
    $row_date = explode(" ",$row[$col_no_date])[0];
    if(!empty($row_date)){
        if(!isset($date_wise_expense[$row_date])){
            $date_wise_expense[$row_date]['amount'] = 0;
            $date_wise_expense[$row_date]['paper_stock'] = 0;
        }
        $date_wise_expense[$row_date]['amount'] += isset($row[$col_no_amount])?(int)$row[$col_no_amount]:0;
        if(isset($row[$col_no_asset]) && $row[$col_no_asset] == 'A4 Sheet'){
            $date_wise_expense[$row_date]['paper_stock'] += isset($row[$col_no_stock])?(int)$row[$col_no_stock]:0;
        }
        // if($row[$col_no_asset] == 'A4 Sheet'){
        //     $all_assets['A4 Sheet']['stock'] = $row[$col_no_stock];
        // }
    }
}
// print_r($date_wise_expense);die;


$paper_stock = 0;
$date_wise_asset_stock = array();
foreach ($period as $dt) {
    $d = $dt->format("d-m-Y");
    $paper_stock += isset($date_wise_expense[$d]['paper_stock'])?(int)$date_wise_expense[$d]['paper_stock']:0;
    $date_wise_asset_stock[$d]['A4 Sheet'] = $paper_stock;
}
// print_r($date_wise_asset_stock);die;


# Sale Entries
$count=0;
$result = array();
$col_no_date = 1;
$col_no_commission= 7;
$col_no_total_amount= 8;
$col_no_printcount= 14; #
$col_no_paper_count= 15; #
$col_no_service_name= 3;
$col_no_quantity= 4;
$col_no_pending_amount=10;
$col_no_payment_responsible_by=11;
$col_no_customer=2;
$col_no_paper_stock=16; #
$col_no_time_duration=17; #
$col_no_computer_operator=12;
$total_pending_amount = 0;
$pending_amount_details = array();
$income_sheet_total_paper_count = 0;
foreach ($income_sheet_rows as $row) {
    $row_date = explode(" ",$row[$col_no_date])[0];
    $count++;
    $computer_operator = isset($row[$col_no_computer_operator]) ? $row[$col_no_computer_operator] : 'Unknown';
    if($count > 0 && !empty($row_date)){
        $payment_responsible_by = 'unknown';
        if(!isset($result[$row_date])){
            $result[$row_date]['commission'] = 0;
            $result[$row_date]['total_amount'] = 0;
            $result[$row_date]['printcount'] = 0;
            $result[$row_date]['paper_count'] = 0;
            $result[$row_date]['pending_amount'] = 0;
            $result[$row_date]['busy_hours'] = array();
            $result[$row_date]['issues'] = array();
        }
        if(!isset($result[$row_date]['issues'][$computer_operator])){
            $result[$row_date]['issues'][$computer_operator] = array();
        }
        $computer_operator = isset($row[$col_no_computer_operator])?$row[$col_no_computer_operator]:'';
        if(!isset($result[$row_date]['busy_hours'][$computer_operator])){
            $result[$row_date]['busy_hours'][$computer_operator] = 0;
        }
        $result[$row_date]['commission'] += isset($row[$col_no_commission])?(int)$row[$col_no_commission]:0;
        $result[$row_date]['total_amount'] += isset($row[$col_no_total_amount])?(int)$row[$col_no_total_amount]:0;
        $result[$row_date]['printcount'] += isset($row[$col_no_printcount])?(int)$row[$col_no_printcount]:0;
        $result[$row_date]['paper_count'] += isset($row[$col_no_paper_count])?(int)$row[$col_no_paper_count]:0;
        $income_sheet_total_paper_count += isset($row[$col_no_paper_count])?(int)$row[$col_no_paper_count]:0;

        $total_pending_amount += isset($row[$col_no_pending_amount])?(int)$row[$col_no_pending_amount]:0;
        $result[$row_date]['pending_amount'] = $total_pending_amount;

       
            
        $result[$row_date]['busy_hours'][$computer_operator] += isset($row[$col_no_time_duration])?(int)$row[$col_no_time_duration]:0;
        
        
        if(isset($row[$col_no_paper_stock]) && !empty($row[$col_no_paper_stock]) && isset($date_wise_asset_stock[$row_date])){
            // echo $date_wise_asset_stock[$row_date]['A4 Sheet'].'<br>';
            // echo $result[$row_date]['paper_count'].'<br>';
            // print_r($result);
            // die;
            $actual_paper_stock  = $date_wise_asset_stock[$row_date]['A4 Sheet'] -  $income_sheet_total_paper_count;
            // echo $actual_paper_stock.'<br>';die;
            $paper_stock_diff = (isset($row[$col_no_paper_stock]) && $row[$col_no_paper_stock] >0  ? $row[$col_no_paper_stock] : 0 ) - $actual_paper_stock;
            if($paper_stock_diff != 0){
                $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Paper stock mismatch '.$paper_stock_diff.'('.$row[$col_no_paper_stock].'-'. $actual_paper_stock.')' ;
            }

        }
        if(empty($row[$col_no_service_name])){
            $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Service Name should not be empty';
            continue;
        }
        if( !isset($all_services[$row[$col_no_service_name]]) ){
            $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Unknown service name';
            continue;
        }
        if(!(isset($row[$col_no_quantity]) && !empty($row[$col_no_quantity])) && $all_services[$row[$col_no_service_name]]['category'] != 'Internal'){
            $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Quantity is required';
            continue;
        }
        $current_row_print_count = isset($row[$col_no_printcount])?(int)$row[$col_no_printcount]:0;
        $current_row_paper_count = isset($row[$col_no_paper_count])?(int)$row[$col_no_paper_count]:0;

        if(in_array($row[$col_no_service_name], ["A4 Xerox - Single Side","Legal xerox - Single Side", "A4 Xerox - Double Side", "Legal xerox - Double Side", "A4  Printout - Single Side", "A4  Printout - Double Side"])){
            if($row[$col_no_quantity] != $current_row_paper_count){
                $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Paper count mismatch';
            }
            // if($row[$col_no_quantity] != $current_row_print_count){
            //     $result[$row_date]['issues'][] = 'Row-'.$count.' : Paper count mismatch';
            // }
        }

        if(in_array($row[$col_no_service_name], ["A4 Xerox - Single Side","Legal xerox - Single Side", "A4  Printout - Single Side"])){
            if($row[$col_no_quantity] != $current_row_print_count){
                $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Print count mismatch';
            }
        }

        if(in_array($row[$col_no_service_name], ["A4 Xerox - Double Side", "Legal xerox - Double Side", "A4  Printout - Double Side"])){
            if( ($row[$col_no_quantity] * 2) != $current_row_print_count){
                $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Print count mismatch';
            }
        }
        $actual_commission = ceil((ceil(((int)$row[$col_no_total_amount]-(int)$row[$col_no_commission])/1000) * (int)$row[$col_no_quantity]) * (float)$all_services[$row[$col_no_service_name]]['commission']);
        
        if( ($row[$col_no_service_name]=='Gpay money transfer' && (int)$actual_commission != (int)$row[$col_no_commission]) or  ($row[$col_no_service_name]!='Gpay money transfer' && (ceil((int)$row[$col_no_quantity] * (float)$all_services[$row[$col_no_service_name]]['commission']) != $row[$col_no_commission]) )){

            
       
            $issue = 'Row-'.$count.' : Commission mismatch '.$actual_commission;
            // echo $row_date;
            // $time = strtotime($row_date);
            // $newformat = date('Y-m-d',$time);
            // echo $newformat;
            // die;
            if(strtotime($row_date) <= strtotime("2024-01-08") ){
                $issue = "<span style='color:orange'>".$issue."</span>";
            }
            if($all_services[$row[$col_no_service_name]]['commission']==0 || in_array($row[$col_no_service_name], ["Train Ticket Booking"])){
                $issue = "<span style='color:orange'>".$issue."</span>";
            }

            if($row[$col_no_service_name]=='Gpay money transfer'){
                $result[$row_date]['issues'][$computer_operator][] = $issue.'('.(int)$row[$col_no_quantity].'X ('.((int)$row[$col_no_total_amount]-(int)$row[$col_no_commission]).'/1000) X '.((float)$all_services[$row[$col_no_service_name]]['commission']).' ='.$row[$col_no_commission].' )';
            }else{

                $result[$row_date]['issues'][$computer_operator][] = $issue.'('.(int)$row[$col_no_quantity].'X'.((float)$all_services[$row[$col_no_service_name]]['commission']).'='.$row[$col_no_commission].' )';
            }
        }
        // if( (int)$row[$col_no_quantity] * ((int)$all_services[$row[$col_no_service_name]]['charges'] + (int)$all_services[$row[$col_no_service_name]]['commission']) != $result[$row_date]['total_amount'] ){
        //     $result[$row_date]['issues'][] = 'Row-'.$count.' : Total mismatch';
        // }

        # Pending payment 
        if(isset($row[$col_no_pending_amount]) && !empty($row[$col_no_pending_amount])){
            $customer_name = 'unknown';
            if( isset($all_customers[$row[$col_no_customer]]) ){
                $customer_name = $row[$col_no_customer];
            }
            if(!isset($row[$col_no_payment_responsible_by])){
                $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Payment Responsible Person should not be empty';
            }else{
                $payment_responsible_by = $row[$col_no_payment_responsible_by];
            }

            if(!isset($pending_amount_details[$payment_responsible_by])){
                $pending_amount_details[$payment_responsible_by]['total_pending_amount']['pending_amount'] = 0;
                $pending_amount_details[$payment_responsible_by]['total_pending_amount']['customer_name'] = 'Total';
                $pending_amount_details[$payment_responsible_by]['total_pending_amount']['date_created'] = '';
                $pending_amount_details[$payment_responsible_by]['total_pending_amount']['borrow_limit'] = isset($all_customers[$payment_responsible_by])?$all_customers[$payment_responsible_by]['borrow_limit']:0;
            }
            if(!isset($pending_amount_details[$payment_responsible_by][$customer_name])){
                $pending_amount_details[$payment_responsible_by][$customer_name]['pending_amount'] = 0;
                $pending_amount_details[$payment_responsible_by][$customer_name]['date_created'] = $row_date;
                $pending_amount_details[$payment_responsible_by][$customer_name]['borrow_limit'] = isset($all_customers[$payment_responsible_by])?$all_customers[$payment_responsible_by]['borrow_limit']:0;
                
            }
            $pending_amount_details[$payment_responsible_by][$customer_name]['customer_name'] = $customer_name;
            // $pending_amount_details[$payment_responsible_by][$customer_name]['borrow_limit'] = $all_customers[$customer_name];
            
            
            $pending_amount_details[$payment_responsible_by][$customer_name]['pending_amount'] += isset($row[$col_no_pending_amount])?(int)$row[$col_no_pending_amount]:0;
            $pending_amount_details[$payment_responsible_by]['total_pending_amount']['pending_amount'] += isset($row[$col_no_pending_amount])?(int)$row[$col_no_pending_amount]:0;
            
        //    print_r($pending_amount_details);die;
        }

        if(empty($row[$col_no_customer])){
            $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Customer Name should not be empty';
        }else{
            if( !isset($all_customers[$row[$col_no_customer]]) ){
                $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Unknown customer name';
            }else{
                // print_r($all_customers[$row[$col_no_customer]]);die;
                if($all_customers[$row[$col_no_customer]]['customer_name'] != 'Unknown'){
                    if(empty($all_customers[$row[$col_no_customer]]['mobile']) ){
                        $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Customer mobile should not be empty';
                    }
                    if(empty($all_customers[$row[$col_no_customer]]['address'])){
                        $result[$row_date]['issues'][$computer_operator][] = 'Row-'.$count.' : Customer address should not be empty';
                    }

                }
            }
        }
    }
}
// print_r($result);die;



# Master Entries
$col_no_date = 0;
$col_no_income = 1;
$col_no_expense = 2;
$col_no_total = 11;
$col_no_calculation_by = 13;
$col_no_machine_print_count = 15;
$col_no_lost_customers = 17;
$master_sheet_result = array();
$master_row_count = 0;
foreach ($master_sheet_rows as $row) {
    $master_row_count++;
    $row_date = explode(" ",$row[$col_no_date])[0];
    if(!empty($row_date)){

        $master_sheet_result[$row_date]['income'] = $row[$col_no_income];
        $master_sheet_result[$row_date]['expense'] = $row[$col_no_expense];
        $master_sheet_result[$row_date]['total'] = $row[$col_no_total];
        $master_sheet_result[$row_date]['calculation_by'] = isset($row[$col_no_calculation_by]) ? $row[$col_no_calculation_by] : '';
        $master_sheet_result[$row_date]['printcount'] = isset($row[$col_no_machine_print_count]) ? $row[$col_no_machine_print_count] : '';
        $master_sheet_result[$row_date]['issues'] = array();
        if(!isset($row[$col_no_lost_customers])){
            $master_sheet_result[$row_date]['issues'][] = "Master Row-".$master_row_count.": Lost customers count is required";
        }
    }
}
// print_r($master_sheet_result);die;



$response = array();
$master_previous_row = array(
    'income' => 0,
    'expense' => 0,
    'total' => 0,
    'calculation_by' => '',
    'printcount' => 0,
); 
$row_count=0; 
foreach($result as $key=>$row){
    $row_count++;
    if( (!isset($master_sheet_result[$key])) || $row_count==1){
        continue; 
    }
    $temp_response = array();
    $temp_response['date'] = $key;
    $temp_response['calculation_by'] = $master_sheet_result[$key]['calculation_by'];
    $temp_response['commission'] = $row['commission'];
    $temp_response['expense'] = isset($date_wise_expense[$key])?$date_wise_expense[$key]['amount']:0;
    $temp_response['pending_amount'] = $row['pending_amount'];

    $actual_total = ($master_previous_row['total'] + $row['commission']) - (isset($date_wise_expense[$key]['amount']) && is_numeric($date_wise_expense[$key]['amount'])?$date_wise_expense[$key]['amount']:0);
    $temp_response['total_amount'] = array(
        'actual_total' => $actual_total,
        'account_total' => $master_sheet_result[$key]['total'],
        'careless' => ($master_sheet_result[$key]['total']-$actual_total)
    );
    // if(!is_numeric($master_previous_row['printcount'])){
    //     print_r($master_previous_row);die;
    // }
    $temp_response['print_count'] = array(
        'machine' => $master_sheet_result[$key]['printcount'],
        'used' => $row['printcount'],
        'careless' => ((!empty($master_sheet_result[$key]['printcount'])? $master_sheet_result[$key]['printcount'] : 0 ) - ( (!empty($master_previous_row['printcount'])?$master_previous_row['printcount']:0) + $row['printcount']))
    );
    $temp_response['busy_hours'] = $row['busy_hours'];
    $temp_response['careless_mistakes'] = $row['issues'];
    if (!array_key_exists($temp_response['calculation_by'],$temp_response['careless_mistakes'])){
        $temp_response['careless_mistakes'][$temp_response['calculation_by']] = array();
    }
    $temp_response['careless_mistakes'][$temp_response['calculation_by']] = array_merge($temp_response['careless_mistakes'][$temp_response['calculation_by']],$master_sheet_result[$key]['issues']);
    $response[] = $temp_response;
    $master_previous_row = isset($master_sheet_result[$key]) ? $master_sheet_result[$key]: '';
}
// print_r($response);die;
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

/* tr:nth-child(even) {
  background-color: #dddddd;
} */
.wrong {
    color: red;
}



.table-container thead {

  background-color: #f2f2f2;
}


</style>
</head>
<body>
<br><br>
<form method="post">
<input type="hidden" name="destroy">
  <input type="submit" value="Refresh">
</form> 
<table style="display:none">
  <tr>
    <th>Authorized Person</th>
    <th>Customer Name</th>
    <th>Pending Amount </th>
  </tr>
  <?php  $previous_customer=""; foreach ($pending_amount_details as $prb=>$customers_with_pending) : ?>
    <?php //krsort($customers_with_pending); ?>
    <?php foreach ($customers_with_pending as $row) : ?>
  <tr>
    <?php  if($previous_customer!=$prb){ ?>
      <td rowspan="<?php echo count($customers_with_pending); ?>">
        <span style="<?php echo ( ($prb=='unknown' || $row['pending_amount']>$row['borrow_limit']) && $prb !='Esakkithas'  ?'color:red':''); ?>">
            Name: <?php echo $prb; ?> <br>
            Limit: <?php echo $customers_with_pending['total_pending_amount']['borrow_limit']; ?><br>
            Pending Amount : <?php echo $row['pending_amount']; ?> <br>
            <a href="https://wa.me/918012707274?text=%E0%AE%85%E0%AE%A9%E0%AF%8D%E0%AE%AA%E0%AF%81%E0%AE%B3%E0%AF%8D%E0%AE%B3%20%E0%AE%B5%E0%AE%BE%E0%AE%9F%E0%AE%BF%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AF%88%E0%AE%AF%E0%AE%BE%E0%AE%B3%E0%AE%B0%E0%AF%87%2C%0A%20%E0%AE%A4%E0%AE%99%E0%AF%8D%E0%AE%95%E0%AE%B3%E0%AF%8D%20%E0%AE%AA%E0%AF%86%E0%AE%AF%E0%AE%B0%E0%AE%BF%E0%AE%B2%E0%AF%8D%20%E0%AE%B0%E0%AF%82.<?php echo $row['pending_amount']; ?>%20%E0%AE%A8%E0%AE%BF%E0%AE%B2%E0%AF%81%E0%AE%B5%E0%AF%88%E0%AE%AF%E0%AE%BF%E0%AE%B2%E0%AF%8D%20%E0%AE%89%E0%AE%B3%E0%AF%8D%E0%AE%B3%E0%AE%A4%E0%AF%81.%20%E0%AE%87%E0%AE%A8%E0%AF%8D%E0%AE%A4%E0%AE%A4%E0%AF%8D%20%E0%AE%A4%E0%AF%8A%E0%AE%95%E0%AF%88%E0%AE%AF%E0%AF%88%20%E0%AE%B5%E0%AE%B0%E0%AF%81%E0%AE%95%E0%AE%BF%E0%AE%B1%2010-%E0%AE%AE%E0%AF%8D%20%E0%AE%A4%E0%AF%87%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AF%81%20%E0%AE%AE%E0%AF%81%E0%AE%A9%E0%AF%8D%20%E0%AE%9A%E0%AF%86%E0%AE%B2%E0%AF%81%E0%AE%A4%E0%AF%8D%E0%AE%A4%E0%AF%81%E0%AE%AE%E0%AE%BE%E0%AE%B1%E0%AF%81%20%E0%AE%95%E0%AF%87%E0%AE%9F%E0%AF%8D%E0%AE%9F%E0%AF%81%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AF%8A%E0%AE%B3%E0%AF%8D%E0%AE%95%E0%AE%BF%E0%AE%B1%E0%AF%8B%E0%AE%AE%E0%AF%8D.%0A%0A%20%20---%E0%AE%B8%E0%AF%8D%E0%AE%B0%E0%AF%80%E0%AE%A8%E0%AE%BE%20%E0%AE%AA%E0%AF%8A%E0%AE%A4%E0%AF%81%20%E0%AE%87-%E0%AE%9A%E0%AF%87%E0%AE%B5%E0%AF%88%20%E0%AE%AE%E0%AF%88%E0%AE%AF%E0%AE%AE%E0%AF%8D%20%E0%AE%B5%E0%AE%B3%E0%AF%8D%E0%AE%B3%E0%AE%BF%E0%AE%AF%E0%AF%82%E0%AE%B0%E0%AF%8D" target="_blank">Whatsapp</a>
        </span> 
      
    </td>
      <?php $previous_customer = $prb; } else { ?>
        <td style="<?php echo $row['pending_amount']==0?'display:none':''; ?>"><span style="<?php echo ($row['customer_name']=='unknown'?'color:red':''); ?>"><?php echo $row['customer_name']; ?></span><br><small><?php echo $row['date_created']; ?></small>
        <br>
        <?php if($prb!="Srina Trust"){?>
            <a href="https://wa.me/918012707274?text=%E0%AE%85%E0%AE%A9%E0%AF%8D%E0%AE%AA%E0%AF%81%E0%AE%B3%E0%AF%8D%E0%AE%B3%20%E0%AE%B5%E0%AE%BE%E0%AE%9F%E0%AE%BF%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AF%88%E0%AE%AF%E0%AE%BE%E0%AE%B3%E0%AE%B0%E0%AF%87%2C%0A%20%E0%AE%A4%E0%AE%99%E0%AF%8D%E0%AE%95%E0%AE%B3%E0%AF%8D%20%E0%AE%AA%E0%AF%86%E0%AE%AF%E0%AE%B0%E0%AE%BF%E0%AE%B2%E0%AF%8D%20%E0%AE%B0%E0%AF%82.<?php echo $row['pending_amount']; ?>%20%E0%AE%A8%E0%AE%BF%E0%AE%B2%E0%AF%81%E0%AE%B5%E0%AF%88%E0%AE%AF%E0%AE%BF%E0%AE%B2%E0%AF%8D%20%E0%AE%89%E0%AE%B3%E0%AF%8D%E0%AE%B3%E0%AE%A4%E0%AF%81.%20%E0%AE%87%E0%AE%A8%E0%AF%8D%E0%AE%A4%E0%AE%A4%E0%AF%8D%20%E0%AE%A4%E0%AF%8A%E0%AE%95%E0%AF%88%E0%AE%AF%E0%AF%88%20%E0%AE%B5%E0%AE%B0%E0%AF%81%E0%AE%95%E0%AE%BF%E0%AE%B1%2010-%E0%AE%AE%E0%AF%8D%20%E0%AE%A4%E0%AF%87%E0%AE%A4%E0%AE%BF%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AF%81%20%E0%AE%AE%E0%AF%81%E0%AE%A9%E0%AF%8D%20%E0%AE%9A%E0%AF%86%E0%AE%B2%E0%AF%81%E0%AE%A4%E0%AF%8D%E0%AE%A4%E0%AF%81%E0%AE%AE%E0%AE%BE%E0%AE%B1%E0%AF%81%20%E0%AE%95%E0%AF%87%E0%AE%9F%E0%AF%8D%E0%AE%9F%E0%AF%81%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AF%8A%E0%AE%B3%E0%AF%8D%E0%AE%95%E0%AE%BF%E0%AE%B1%E0%AF%8B%E0%AE%AE%E0%AF%8D.%0A%0A%20%20---%E0%AE%B8%E0%AF%8D%E0%AE%B0%E0%AF%80%E0%AE%A8%E0%AE%BE%20%E0%AE%AA%E0%AF%8A%E0%AE%A4%E0%AF%81%20%E0%AE%87-%E0%AE%9A%E0%AF%87%E0%AE%B5%E0%AF%88%20%E0%AE%AE%E0%AF%88%E0%AE%AF%E0%AE%AE%E0%AF%8D%20%E0%AE%B5%E0%AE%B3%E0%AF%8D%E0%AE%B3%E0%AE%BF%E0%AE%AF%E0%AF%82%E0%AE%B0%E0%AF%8D" target="_blank">Whatsapp</a>
        <?php } ?>
    </td>
        <td style="<?php echo $row['pending_amount']==0?'display:none':''; ?>">
        <span style="<?php echo ($row['pending_amount']>$row['borrow_limit']?'color:red':''); ?>">
        <?php echo $row['pending_amount']; ?></span>
       
    </td>
    <?php } ?>
    </tr>
    <?php endforeach; ?>
  <?php endforeach; ?>
</table>
<br><br>
<div class="table-container">
<table>
    <thead>
    <tr>
        <th rowspan="2">Date</th>
        <th rowspan="2">Calculation By</th>
        <th rowspan="2">Commission</th>
        <th rowspan="2">Expense</th>
        <th rowspan="2">Pending</th>
        <th colspan="3">Total Amount</th>
        <th colspan="3">Print Count</th>
        <th rowspan="2">Busy Hours</th>
        <th rowspan="2">Careless Mistakes</th>
    </tr>
       
    <tr>
        <th>Account</th>
        <th>Actual</th>
        <th>Careless</th>
        <th>Machine</th>
        <th>Used</th>
        <th>Careless</th>
    </tr>
    </thead>
    <?php foreach($response as $row){ ?>
    <tr>
    <td><?php echo $row['date']; ?> </td>
    <td><?php echo $row['calculation_by']; ?> </td>
    <td><?php echo $row['commission']; ?> </td>
    <td><?php echo $row['expense']; ?> </td>
    <td><?php echo $row['pending_amount']; ?></td>
    <td><?php echo $row['total_amount']['account_total']; ?></td>
    <td><?php echo $row['total_amount']['actual_total']; ?></td>
    <td class="<?php echo ($row['total_amount']['careless']==0?'correct': 'wrong'); ?>"><?php echo $row['total_amount']['careless']; ?></td>
    <td><?php echo $row['print_count']['machine']; ?></td>
    <td><?php echo $row['print_count']['used']; ?></td>
    <td class="<?php echo $row['print_count']['careless']==0? 'correct': 'wrong'; ?>"><?php echo $row['print_count']['careless']; ?></td>
    <td>
    <?php foreach($row['busy_hours'] as $operator=>$busy_hour){ ?>
        <?php echo $operator; ?> : <?php echo round($busy_hour/60, 2); ?><br>
            <?php } ?>
    </td>
    <td>
        
        <ul style="color: red">
            <?php foreach($row['careless_mistakes'] as $employee_name=>$employee_issue){ ?>
<?php if(count($employee_issue)>0){ ?>
                <li style="list-style: none;"><u><?php echo $employee_name; ?></u></li>
                <?php foreach($employee_issue as $issue){ ?>
            <li><?php echo $issue; ?></li>
            <?php } ?>
            <?php } ?>

            <?php } ?>
    
        </ul>


    </td>
    </tr>
  <?php } ?>
</table>
                </div>
</body>
</html>

