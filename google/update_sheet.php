<?php 
if(!$_POST){
    die;
}
// print_r($_POST['expenses']);die;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// $_POST['spreadsheet_id'] = '1-5TXsWZI_uJCEHU3pSEuDZsnncaJJrzuWHsm7OdZZjQ';
// $_POST['customers'] = [['rrr1']];
// $_POST['expenses'] = [['rrr2']];
// $_POST['assets'] = [['rrr3']];
// $_POST['services'] = [['rrr4']];
// $_POST['master'] = [['rrr5']];
// $_POST['sale'] = [['rrr6']];
require 'vendor/autoload.php'; 
use Google\Client;
use Google\Service\Sheets;


$input_data = file_get_contents("data.json");
$input_data = json_decode($input_data, true);


$client = new \Google_Client();
$client->setApplicationName('Google Sheets API');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$path = 'credentials.json';
$client->setAuthConfig($path);
$service = new \Google_Service_Sheets($client);

function writeGoogleSheet($service, $spreadsheetId, $range, $data){
    $requestBody = new \Google\Service\Sheets\ValueRange([
        'values' => $data
    ]);
    $response = $service->spreadsheets_values->update($spreadsheetId, $range, $requestBody, [
        'valueInputOption' => 'RAW'
    ]);
    if ($response->updatedCells > 0) {
        echo 'success';
    } else {
        echo 'failure';
    }
}

if(isset($input_data['customers'])){
    //!A1:O1
    writeGoogleSheet($service, $input_data['spreadsheet_id'], 'Customers', $input_data['customers']);
}

if(isset($input_data['expenses'])){
    writeGoogleSheet($service, $input_data['spreadsheet_id'], 'Expense', $input_data['expenses']);
}

if(isset($input_data['assets'])){
    writeGoogleSheet($service, $input_data['spreadsheet_id'], 'Assets', $input_data['assets']);
}

if(isset($input_data['services'])){
    writeGoogleSheet($service, $input_data['spreadsheet_id'], 'Services', $input_data['services']);
}

if(isset($input_data['master'])){
    writeGoogleSheet($service, $input_data['spreadsheet_id'], 'Master', $input_data['master']);
}

if(isset($input_data['sale'])){
    writeGoogleSheet($service, $input_data['spreadsheet_id'], 'Sale', $input_data['sale']);
}


