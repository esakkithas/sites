<?php 

namespace App\Models;  
use CodeIgniter\Model;

  
class UserModel extends Model{

    protected $table = 's_users';
    
    protected $allowedFields = [
        'name',
        'email',
        'password',
        'status',
        'created_at'
    ];

}