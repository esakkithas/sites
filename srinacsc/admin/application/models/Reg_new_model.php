<?php
class Reg_new_model extends CI_Model{

  public function insert($talename,$data){    
    $this->db->insert($talename,$data);
    return $this->db->insert_id();
  }

  public function select($tablename){
  	return $this->db->get($tablename)->result_array();
  }

  public function selectrow($id){
  	$this->db->where('id',$id);
  	return	$this->db->get('reg_form')->row_array();
  }

  public function update($updateid,$data){
  	$this->db->where('id',$updateid);
  	$this->db->update('reg_form',$data);
  }

  public function delete($id){
  	$this->db->where('id',$id);
  	$this->db->delete('reg_form');
  }

 
}

