
          <h2>Income</h2>
          <table class="table" >
            <thead>
              <tr>
                <th>Date</th>
                <th>Service</th>
                <th>Customer</th>
                <th>Quantity</th>
                <th>Charges</th>
                <th>Price</th>
               <th>Amount</th>
              </tr>
              </thead>
              <tbody>
              <?php 
              $total_price = 0;
              $total_amount = 0;
              
              foreach($list as $l): 
                $temp_total_price = ceil($l['quantity'] * $l['sii_commission']);
                $total_price += $temp_total_price;
                $total_amount += ceil($l['total_amount']);
                // $total_amount += ceil($l['quantity'] * ( $l['charges'] + $l['sii_commission']));
              ?>
              <tr>
                <td><?php echo $l['datetime']; ?></td>
                <td><?php echo $l['service_name']; ?></td>
                <td><?php echo $l['customer_name']; ?></td>
                <td><?php echo $l['quantity']; ?></td>
                <td><?php echo $l['charges']; ?></td>
                <td title="<?php echo $temp_total_price; ?>"><?php echo $l['sii_commission']; ?></td>
                <td><?php echo ceil($l['total_amount']); ?></td>
              </tr>
              <?php endforeach; ?>
              <tfoot>
              <tr>
                <th colspan="5">Total</th>
                <th><?php echo $total_price; ?></th>
               <th><?php echo $total_amount; ?></th>
              </tr>
              </tfoot>
          </table>