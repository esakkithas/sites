<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Expense</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Expense</li>
            </ol>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
            <div class="card">
              <h2></h2>
              <div class="container">
                <!-- Button to Open the Modal -->
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">
               Add+
             </button>   
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="expense-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Datetime</th>
                    <th>Expense Type</th>
                    <!-- <th>Stock</th> -->
                   <th>Amount</th>
                   <!-- <th>additional_info</th> -->
                   <th>Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                 
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
                
              </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
 <!-- Add popup - start -->
      <div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Expense</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="addexpense(); return false" method="post">

<div class="form-group">
    <label for="uname">Date*:</label>
    <input type="date" class="form-control" id="uname" placeholder="Date" name="datetime" value="<?php echo date('Y-m-d'); ?>" required>
    <div class="valid-feedback">Valid.</div>
  </div>

<div class="form-group">
    <label for="pswd">Expense Type*:</label>
  <select class="form-control" name="expense_type" onchange="expenseTypeChanges()" required>
    <option value="">Select</option>
  <option value="1">Purchase Entry</option>
  <!-- <option value="1">Assets Entry</option> -->
  <option value="0">Shop Expenses</option>
  </select>
  </div>

  <div class="form-group product-sec">
    <label for="pswd">Product*:</label>
    <input list="productsList"  class="form-control" id="products-list">
  <datalist id="productsList">
  <?php foreach($productsList as $prod): ?>
  <option data-value="<?php echo $prod['id']; ?>" value="<?php echo $prod['name']; ?>"><?php echo $prod['name']; ?> </option>
  <?php endforeach; ?>
  </datalist>
  <input type="hidden" name="product_id" id="products-list-hidden">
  </div>

  <div class="form-group asset-sec">
    <label for="pswd">Assets*:</label>
    <input list="assetsList"  class="form-control" id="assets-list">
  <datalist id="assetsList">
  <?php foreach($assetsList as $asset): ?>
  <option data-value="<?php echo $asset['id']; ?>" value="<?php echo $asset['name']; ?>"><?php echo $asset['name']; ?> </option>
  <?php endforeach; ?>
  </datalist>
  <input type="hidden" name="asset_id" id="assets-list-hidden">
  </div>

  <div class="form-group expense-details-sec">
    <label for="pswd">Expense Details*:</label>
  <select class="form-control" name="id_master_expense">
    <option value="">Select</option>
  <?php foreach($masterExpensesList as $expense): ?>
  <option value="<?php echo $expense['id']; ?>"><?php echo $expense['name']; ?> </option>
  <?php endforeach; ?>
  </select>
  </div>

  <div class="form-group stock-sec">
    <label for="pwd">Stock:</label>
    <input type="number" class="form-control" name="count" value="0" required>
  </div>
  <div class="form-group">
    <label for="pwd">Amount*:</label>
    <input type="number" class="form-control"  name="amount" required>
  </div>
  <div class="form-group">
    <label for="pswd">Payment Method*:</label>

  <select id="assetsList" class="form-control" name="account_id" required>
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>"><?php echo $acc['display_name'] ? $acc['display_name'] : $acc['account_name']; ?> </option>
  <?php endforeach; ?>
  </select>
  </div>

  <div class="form-group">
    <label for="pwd">Additional Info:</label>
    <input type="text" class="form-control" id="pwd"  name="additional_info">
  </div>
  <div class="ab">
  <input type="submit"value="submit" class="btn btn-primary" >
  </div>
</form>
      </div>
      </div>
              </div>
            </div>

            <div class="modal" id="view1">
            <div class="modal-dialog">
              <div class="modal-content">
           <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <td class="id"></td>  
                </tr>
                <tr>
                  <th>Date</th>
                  <td class="datetime"></td>
                </tr>
                <tr>
                  <th>Expense Type</th>
                  <td class="expense_type"></td>
                </tr>
                <tr class="expense-tr">
                  <th>Expense</th>
                  <td class="expense"></td>
                </tr>
                <tr class="product-tr">
                  <th>Product</th>
                  <td class="asset_name"></td>
                </tr>
                <tr class="stock-tr">
                  <th>Stock</tH>
                  <td class="count"></td>
                </tr>
                <tr>
                  <th>Amount</th>
                  <td class="amount"></td>
                </tr>
                <tr>
                  <th>Additional Info</th>
                  <td class="additional_info"></td>
                </tr>
                </thead>
                <tbody>
                <tr>

                  </tr>
                  </tbody>
                  </table>
                  </div>
                  </div>
                  </div>
                  
                  </div>
<?php $this->load->view('footer'); ?>