<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customers</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Customers</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
            <div class="card">
                <h3></h3>
              <div class="container">
                <!-- Button to Open the Modal -->
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">
               Add+
             </button>   
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="customers-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th style="width:10%">Customer Id</th>
                    <th>Name</th>
                    <th>Mobile</th>
                   <th>Address</th> 
                   <th>Recently At</th> 
                   <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>





      <div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Customer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="addcustomers(); return false" method="post">
  <div class="form-group">
    <label for="uname">Name*</label>
    <input type="" class="form-control" id="uname" placeholder="" name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Mobile*:</label>
    <input type="number" class="form-control" id="pwd" placeholder="" name="mobile" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="form-group">
    <label for="uname">Customer Id</label>
    <input type="text" class="form-control"  placeholder="" name="customer_id">
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="uname">Notification</label>
    <select class="form-control"  name="notification">
      <option value="No">No</option>
      <option value="Yes">Yes</option>
  </select>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="uname">Borrow Limit</label>
    <input type="number" class="form-control"  value="0" name="borrow_limit" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label>Referred by:</label>
    <select name="referred_by" class="form-control" >
    <option>Select</option>
  <?php foreach($customers as $customer): ?>
  <option value="<?php echo $customer['id']; ?>"><?php echo $customer['name']; ?> </option>
  <?php endforeach; ?>
  </select>
  </div>
  <div class="form-group">
    <label>Address*:</label>
    <textarea class="form-control" name="address" required></textarea>
  </div>
  <?php if($ENABLE_GST) { ?> 
    <div class="form-group">
      <label>GSTIN:</label>
      <input type="text" class="form-control" name="gstin">
    </div> 
    <div class="form-group">
      <label>State:</label>
      <input type="text" class="form-control" name="state">
    </div> 
    <div class="form-group">
      <label>State Code:</label>
      <input type="text" class="form-control" name="state_code">
    </div> 
    <div class="form-group">
      <label>Email:</label>
      <input type="text" class="form-control" name="email">
    </div> 
  <?php } ?>
  <div class="ab">
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>






      <div class="modal" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Customer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="confirmEditCustomer(); return false" method="post">
<input type="hidden" class="form-control" name="id" required>
  <div class="form-group">
    <label for="uname">Customer Name*</label>
    <input type="text" class="form-control"  placeholder="" name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="uname">Customer Id*</label>
    <input type="text" class="form-control"  placeholder="" name="customer_id" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="uname">Notification</label>
    <select class="form-control"  name="notification">
      <option value="No">No</option>
      <option value="Yes">Yes</option>
  </select>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="uname">Borrow Limit</label>
    <input type="number" class="form-control"  value="0" name="borrow_limit" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label>Referred by:</label>
    <select name="referred_by" class="form-control" >
    <option>Select</option>
  <?php foreach($customers as $customer): ?>
  <option value="<?php echo $customer['id']; ?>"><?php echo $customer['name']; ?> </option>
  <?php endforeach; ?>
  </select>
  </div>
  <div class="form-group">
    <label for="pwd">Mobile*:</label>
    <input type="number" class="form-control" id="pwd" placeholder="" name="mobile" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>

  <!-- <div class="form-group" style="display:none">
    <label for="pwd">Referred by:</label>
    <input type="text" list="customersList" class="form-control"   id="referred_by">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
    <datalist id="customersList">
  <?php //foreach($customers as $customer): ?>
  <option data-value="<?php //echo $customer['id']; ?>" data-name="<?php //echo $customer['name']; ?>" ><?php //echo $customer['name']; ?> </option>
  <?php //endforeach; ?>
    </datalist>
    <input type="hidden" name="referred_by" id="referred_by-hidden">
  </div> -->

  <div class="form-group">
    <label for="pwd">Address*:</label>
    <textarea class="form-control" name="address" required> </textarea>
  </div>
  <?php if($ENABLE_GST) { ?> 
    <div class="form-group">
      <label>GSTIN:</label>
      <input type="text" class="form-control" name="gstin">
    </div> 
    <div class="form-group">
      <label>State:</label>
      <input type="text" class="form-control" name="state">
    </div> 
    <div class="form-group">
      <label>State Code:</label>
      <input type="text" class="form-control" name="state_code">
    </div> 
    <div class="form-group">
      <label>Email:</label>
      <input type="text" class="form-control" name="email">
    </div> 
  <?php } ?>
  <div class="msg"></div>
  <div class="ab">
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>



              <div class="modal" id="view1">
            <div class="modal-dialog">
              <div class="modal-content">
                 <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <td class="id"></td>
                </tr>
                <tr>
                  <th>Customer Id</th>
                  <td class="customer-id"></td>
                </tr>
                <tr>
                <th>Name</th>
                <td class="name"></td>
              </tr>
              <tr>
                <th>mobile</th>
                <td class="mobile"></td>
              </tr>
              <tr>
              <th>address</th>
              <td class="address"></td>

             </tr>
             <tr>
              <th>Referred By</th>
              <td class="referred_by"></td>

             </tr>
                </thead>
                <tbody>
                  </tbody>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>

<?php $this->load->view('footer'); ?>