
<style>
    * {
    font-size: 12px;
    }
    table.bottomBorder { 
    border-collapse: collapse; 
    }
    table.bottomBorder td, 
    table.bottomBorder th { 
    border-bottom: 1px solid #000; 
    padding: 10px; 
    text-align: right;
    }
</style>
<br><br><br>
<table border="1" cellpadding="5">
<tr>
<td style="width: 70%" rowspan="2" >
Buyer (Bill to)
<p><strong><?php echo $invoice->customer_name; ?></strong> <br><?php echo $invoice->address; ?>,
<?php echo $invoice->gstin ? 'GSTIN : '. $invoice->gstin . ', ' : ''; ?>
<?php echo $invoice->state ? 'State Name : '. $invoice->state. ', ' : ''; ?>
<?php echo $invoice->state_code ? 'Code : '. $invoice->state_code. ', ' : ''; ?>
<?php echo $invoice->email ? 'Email : '. $invoice->email. ', ' : ''; ?></p>
</td>
<?php
    function prependZero($number) {
        if ($number < 9){
            return "0" . $number;
        }else{
            return $number;
        } 
    }
    $invoice_time = strtotime($invoice->datetime);
    $year_start_time = strtotime(date("Y-04-01", $invoice_time));
    if($invoice_time > $year_start_time){
        $financial_year = date("y", $invoice_time);
        $financial_year = $financial_year . "-" . ($financial_year + 1);
    }else{
        $financial_year = date("y", $invoice_time);
        $financial_year = ($financial_year - 1) . "-" . $financial_year;
    }
?>
<td style="width: 15%">Invoice Id: <br><strong> <?php echo ($invoice_prefix ? $invoice_prefix.'/' : ''); ?><?php echo $financial_year; ?>/<?php echo $invoice->income_id; ?> </strong></td>
<td style="width: 15%">Invoice Date: <br><strong><?php echo date("d/m/Y", $invoice_time); ?></strong></td>

</tr>
<tr>
<td>Mode of Payment: <br><strong> </strong></td>
<td > Buyer's Order No: <br><strong> </strong></td>
</tr>
</table>
<br><br><br><br>

<table class="bottomBorder" cellpadding="5">
<tr border="1">
<th style="width:8%"><strong>Sl No.</strong></th>
<th style="width:<?php echo $invoice->gst_bill ? '37': '62' ?>%"><strong>Description of Goods</strong></th>
<?php if($invoice->gst_bill) { ?>
<th style="width:15%"><strong>HSN/SAC</strong></th>
<?php } ?>
<th style="width:5%"><strong>Qty</strong></th>
<th style="width:10%"><strong>Rate</strong></th>
<?php if($invoice->gst_bill) { ?>
<th style="width:10%"><strong>GST</strong></th>
<?php } ?>
<th style="width:15%"><strong>Amount</strong></th>
</tr>
<?php
$slno= 0;
$gst_total = 0;
foreach($services as $service){  
$gst_total += $service['gst_value'];
$slno++; ?>
<tr>
<td><?php echo $slno; ?></td>
<td><?php echo $service['display_name'] ? $service['display_name'] : $service['service_name']; ?></td>
<?php if($invoice->gst_bill) { ?>
<td><?php echo $service['hsn']; ?> <?php echo $service['gst_rate'] > 0 ? prependZero($service['gst_rate']). '%' : ''; ?></td>
<?php } ?>
<td><?php echo $service['quantity']; ?></td>
<td><?php echo number_format($service['commission_new'], 2); ?></td>
<?php if($invoice->gst_bill) { ?>
<td><?php echo number_format($service['gst_value'], 2); ?></td>
<?php } ?>
<td><?php echo number_format($service['total_amount'], 2); ?></td>
</tr>
<?php } ?>
<!-- <tr>
<td colspan="6"></td>
<td>275.45</td>
</tr> -->
<tr>
<td></td>
<td></td>
<td colspan="<?php echo $invoice->gst_bill ? '3': '2' ?>">
    <?php if($invoice->gst_bill) { ?>
    <?php if($gst_total > 0){ 
        $half_igst = number_format($gst_total/2, 2);
    ?>
    CGST : Rs. <?php echo $half_igst; ?> <br>SGST : Rs. <?php echo $half_igst; ?>
    <?php } ?>
    <?php } ?>
</td>
<?php if($invoice->gst_bill) { ?>
<td><?php echo number_format($gst_total, 2); ?></td>
<?php } ?>
<td>Rs. <?php echo number_format($invoice->bill_amount, 2); ?></td>
</tr>
<!-- <tr>
<td></td>
<td colspan="5">Total</td>
<td>275.45</td>
</tr> -->
</table>

<br><br><br><br><br><br>



<?php if($tenant_bank_account){ ?>
<table width="50%"border="1" cellpadding="2">
<tr>
<th >A/c Holder's Name:</th>
<td><?php echo $tenant_bank_account->account_holder_name ?></td>
</tr>
<tr>
<th >Bank Name</th>
<td><?php echo $tenant_bank_account->bank_name ?></td>
</tr>
<tr>
<th >A/c No.</th>
<td><?php echo $tenant_bank_account->account_no ?></td>
</tr>
<tr>
<th>Branch & IFS Code</th>
<td><?php echo $tenant_bank_account->branch ?> & <?php echo $tenant_bank_account->ifsc ?></td>
</tr>
</table>
<?php } ?>
<p>Declaration: 
We declare that this invoice shows the actual price of the
goods described and that all particulars are true and correct.</p>

<p style="text-align: right;"><strong>for <?php echo strtoupper($tenant->tenant_name); ?></strong></p>
