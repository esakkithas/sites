
          <h4>Pending Amount Details</h4>
          <table class="table" >
            <thead>
              <tr>
                <th>Customer</th>
                <th>Date</th>
                <th>Income Id</th>
                <th>Bill Amount</th>
                <th>Pending Amount</th>
                <th>Total</th>
              </tr>
              </thead>
              <tbody>
              <?php 


              foreach($list as $l): ?>
          
              <tr>
                <?php if($l['rowspan']>0) { ?>
                <td rowspan="<?php echo $l['rowspan']; ?>"><?php echo $l['customer_name']; ?></td>
           <?php } ?>
                <td><?php echo $l['income_datetime']; ?></td>
                <td><?php echo $l['income_id']; ?></td>
                <td><?php echo ceil($l['bill_amount']); ?></td>
                <td><?php echo ceil($l['pending_amount']); ?></td>
                <?php if($l['rowspan']>0) { ?>
                        <td rowspan="<?php echo $l['rowspan']; ?>"><?php echo $customer_pending_amount[$l['customer_id']]; ?></td>
                <?php } ?>
       
              </tr>
              <?php endforeach; ?>
              <tfoot>
                <tr>
                <th colspan="5">Total</th>
                <th>Rs. <?php echo $total_pending_amount; ?></th>
                </tr>
            </tfoot>
          </table>