<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<style>
  .pagination-div a {
    margin: 9px;
}
.pagination-div{
  padding-left: 20px;
}
  </style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
              <div class="card">
                <h2></h2>
                <div class="container">
                  <img src="../assets/spinner.gif" width="40" class="update-google-sheet-loader" style="display:none">
                <input type="submit" onclick="updateGoogleSheet(); return false" class="btn btn-primary update-google-sheet-btn" value="Update Google Sheet">          
              </div>
              <!-- /.card-header -->
              <div class="card-body">
      
                <table id="example11" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th rowspan="2">Date</th>
                    <th rowspan="2">Sale</th>
                    <th rowspan="2">Expence</th>
                    <th colspan="<?php echo count($accounts) + 1; ?>" class="text-center">Account Balance</th>
                  </tr>
                  <tr>
                  
                     <?php foreach($accounts as $acc): ?>
                    <th><?php echo $acc['account_name']; ?></th>
                    <?php endforeach; ?>
                    <th>Pending</th>
                    <th>Total</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $l): ?>
                  <tr>
                    <td><?php echo $l['transaction_date']; ?></td>
                   <td><button type="button" class="btn btn-primary" onclick="viewIncomeDetails('<?php echo $l['transaction_date']; ?>'); return false">
                    <?php echo number_format((float)$t->getIncomeTotal($l['transaction_date']), 2, '.', ''); ?> </button></td>
                    <td> <button type="button" class="btn btn-primary" onclick="viewExpenseDetails('<?php echo $l['transaction_date']; ?>'); return false">
                    <?php echo number_format((float)$t->getExpenseTotal($l['transaction_date']), 2, '.', ''); ?>
                   </button></td>
                   <?php 
                   $account_balance_details = $t->getAccountBalance($accounts, $l['transaction_date']);
                   foreach($accounts as $acc): ?>
                    <td><a href="#" onclick="getTransactionDetails('<?php echo $l['transaction_date']; ?>', '<?php echo $acc['id']; ?>')"><?php echo number_format((float)$account_balance_details[$acc['id']], 2, '.', ''); ?></a></td>
                    <?php endforeach; ?>
                    <td><a href="#" onclick="getTransactionDetails('<?php echo $l['transaction_date']; ?>', '0')"><?php echo number_format((float)$t->getPendingAmount($l['transaction_date']), 2, '.', ''); ?></a></td>
                    <td><?php echo number_format((float)$account_balance_details['total'] + $t->getPendingAmount($l['transaction_date']), 2, '.', ''); ?></td>
                  </tr>
                  <?php endforeach; ?>
               
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <div class="pagination-div">
              <?php echo $pagination; ?>
                   </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal" id="income">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">

           </div>
         </div>
         </div>
        </div>


        <div class="modal" id="transactions">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">

           </div>
         </div>
         </div>
        </div>


       <div class="modal" id="expence">
        <div class="modal-dialog">
        <div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
  <h4 class="modal-title"></h4>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<!-- Modal body -->
<div class="modal-body">
 
  </div>
  </div> 
  </div>
  </div>


<?php $this->load->view('footer'); ?>