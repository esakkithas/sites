<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<style>
  
#serviceslisttbl input {
  width:150px;
}
form thead{
  background: rgba(0,0,0,.05);
}
.add-income-form tbody input, .edit-income-form tbody input{
  width: 100% !important;
}
.payment-history-form-div {
  padding: 30px 20px;
}
.payment-history-form-div td {
  padding: 5px;
}
  </style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Sale</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Sale</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <h3></h3>
            <form>
     <div class="row" style="margin-left: 20px;">
      
     <div class="col-md-3">
            <input type="date" class="form-control" value="<?php echo isset($_GET['from_date']) ? $_GET['from_date'] : ''; ?>"  name="from_date">
            </div>
            <div class="col-md-3">
            <input type="date" class="form-control"  value="<?php echo isset($_GET['to_date']) ? $_GET['to_date'] : ''; ?>" name="to_date">
            </div>
            <div class="col-md-3">
       
       <!-- Button to Open the Modal -->
  <input type="submit"  class="btn btn-primary" value="Submit">
    

   </div>

            <div class="col-md-3">
       
                <!-- Button to Open the Modal -->
           <button type="button" style=" float: right;margin-right: 20px;" class="btn btn-primary" data-toggle="modal" data-target="#add1">
               Add+  </button>
         
            </div>

            
</div>
</form>
             

               <!-- /.card-header -->
              <div class="card-body">
                <table id="income-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Datetime</th>
                    <th>Customer</th>
                    <th>Bill Amount</th>
                    <th>Payment Status</th>
                   <th>Action</th> 
                  </tr>
                  </thead>
                  <tbody>
  
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- The Modal -->
          
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<!-- ./wrapper -->
<div class="modal" id="add1">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
      <h4 class="modal-title">Add Sale Entry 
       <?php if($ENABLE_GST) { ?>  
        <br> <small> GST Bill </small> <input type="checkbox" onchange="calculation('#add1')"  name="gst_bill">
      <?php } ?>
      </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="addincome(); return false" method="post" class="add-income-form">

  <div class="">
    <table id="serviceslisttbl"  class="serviceslisttbl table">
      <thead>
      <tr>
        <th style="width:25%">Service/Product*</th>
        <th style="width:10%">Quantity*</th>
        <th class="charges-hide-show" style="display:none">Charges</th>
        <th class="charges-hide-show" style="display:none">Charges Account</th>
        <th >Price*</th>
        <th class="gst-rate-hide-show" style="display:none">GST Rate</th>
        <th >Amount</th>
        <th style="display:none">status*</th>
        <th style="display:none">ref_no</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      <tr>
          <td>
            <input type="text" list="servicesList" class="service-name" id="service-name" required>
            <input type="hidden"  name="service_name[]" class="service-name-hidden" id="service-name-hidden">
          </td>
          <td><input type="number" onchange="calculation('#add1')" class="quantity"  name="quantity[]" step="0.01" required></td>
          <td class="charges-hide-show" style="display:none"><input type="number" onchange="calculation('#add1')" name="charges[]" class="charges"  required></td>
          <td class="charges-hide-show" style="display:none">  <select class="form-control fees_account_id" name="charges_debit_account[]" >
              <option value="0">Select</option>
            <?php foreach($accountsList as $acc): ?>
            <option value="<?php echo $acc['id']; ?>" <?php // echo ($acc['id']==1?'selected':''); ?> ><?php echo $acc['account_name']; ?> </option>
            <?php endforeach; ?>
            </select>
          </td>
          <td><input type="number" id="pwd" class="commission"  onchange="calculation('#add1')" step="0.01" name="commission[]" required> </td>
          <td class="gst-rate-hide-show" style="display:none">Rs.<span class="row-gst-amount"  title=""> 0 </span>
              <input type="hidden" class="row-gst-rate" name="gst_rate[]" value="0">
          </td>
          <td><span class="row-total-amount">Rs. 0</span></td>
          <td style="display:none">
                <select name="status[]" class="custom-select1">
            <option value="">Select</option>
            <option value="Pending">Pending</option>
            <!-- <option value="Inprogress">Inprogress</option> -->
            <option value="Done" selected>Done</option>
          </select>
          </td>
          <td style="display:none"><input type="text" class="refno" id="pwd" placeholder="" name="ref_no[]" ></td>
          <td> &nbsp; <a href="#" onclick="addRow(this, '#add1'); return false"><i class="fas fa-plus"></i></a> &nbsp;
            <a href="#" onclick="deleteRow(this, '#add1'); return false"><i class="fas fa-trash"></i></a> 
          </td>
         </tr>
        </tbody>
        <thead>
      <tr>
        <th colspan="3" class="total-text">Total</th>
        <th class="gst-rate-hide-show total-gst-rate" style="display:none"></th>
        <th><span class="total-bill-amount">Rs. 0</span></th>
        <th></th>
      </tr>
      </thead>
        </table>
        <datalist id="servicesList">
  <?php foreach($services as $service): ?>
  <option data-value="<?php echo $service['id']; ?>" data-fees_account_id="<?php echo $service['fees_account_id']; ?>" data-igst="<?php echo isset($service['igst']) ? $service['igst'] : 0; ?>" data-hsn="<?php echo $service['hsn']; ?>" data-charges="<?php echo $service['fees']; ?>" data-comm="<?php echo $service['commission']; ?>" value="<?php echo $service['name']; ?>"><?php echo $service['barcode'] ? $service['barcode'] : $service['name']; ?></option>
  <?php endforeach; ?>
    </datalist>
  <hr>  
</div>

<div class="form-group">
    <label for="uname">Date (dd/mm/yyyy hh:mm AM/PM)*:</label>
    <input type="datetime-local" class="form-control"  id="addincome-datetime" name="datetime" value="<?php echo date('Y-m-d\TH:i'); ?>" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Customer*:</label>
    <input list="Customerlist" id="customer-id" name="customer_id"class="custom-select mb-2" required>
  <datalist id="Customerlist">
  <?php foreach($customers as $customer): ?>
  <option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['mobile']; ?></option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="customer_id" id="customer-id-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>

      <div class="form-group" style="display:none">
  <label for="pwd">Total Amount*:</label>
  <input type="hidden" class="form-control bill-amount" id="pwd" step="0.01" placeholder="" name="bill_amount" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>

<div class="form-group">
  <label for="pwd">Payment Status*:</label>
  <select name="payment_status"   class="payment_status custom-select" onchange="paymentStatusChanges()" required> 
    <option value="">Select</option>
    <option value="0">Pending</option>
    <option value="1" selected>Paid</option>
    <option value="2">Partial Payment</option>
  </select>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>    
</div>
<div class="form-group paid-amt">
  <label for="pwd">Amount*:</label>
  <input type="number" class="form-control pending-amount" step="0.01" name="paid_amount" value="0" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>




<div class="form-group payment-responsible-by">
    <label for="pwd">Payment Responsible By:</label>
    <input list="Customerlist1" class="custom-select mb-2" id="payment_responsible_by">
  <datalist id="Customerlist1">
  <?php foreach($customers as $customer): ?>
  <option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="payment_responsible_by" id="payment_responsible_by-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
<div class="form-group credit-account">
    <label for="pswd">Payment Method*:</label>

  <select id="assetsList" class="form-control" name="account_id" requird>
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>"  class="<?php echo strtolower($acc['account_name']); ?>" <?php echo ($acc['account_name'] == 'Shop' ? 'selected': ''); ?>><?php echo $acc['display_name']?$acc['display_name']:$acc['account_name']; ?></option>
  <?php endforeach; ?>
  </select>

  </div>

  <div class="form-group" style="display:none">
    <label for="uname">Paper Count(A4)*</label>
    <input type="number" class="form-control"   name="paper_count" value="0" required>
    <div class="valid-feedback">Valid.</div>
  </div>

  <div class="form-group">
    <label for="">Comments</label>
    <input type="text" class="form-control"   name="comments">
    <div class="valid-feedback">Valid.</div>
  </div>
  
    <div class="ad"><p class="msg" style="color:green;float:left"></p>
    <input type="submit"value="submit" class="btn btn-primary">
    </div>             
        </div>
        </div> 
      </form>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>



<!-- ./wrapper -->
<div class="modal" id="editForm">
  
<div class="modal-dialog modal-xl">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Sale Entry 
        <?php if($ENABLE_GST) { ?>  
        <br> 
        <small> GST Bill </small> <input type="checkbox" onchange="calculation('#editForm')"  name="gst_bill">
        <?php } ?>
      </h4>

 
          
  
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>

        <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home" onclick="incomeTab(this); return false">Edit</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1" onclick="incomeTab(this); return false">Payment History</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="home" class=" tab-pane active"><br>
      

<form id="edit-sale" class="edit-income-form" onsubmit="confirmEditIncome(); return false" method="post">

  
<div class="">


  <table id="serviceslisttbl"  class="serviceslisttbl table">
    <thead>
    <tr>
        <th style="width:25%">Service/Product*</th>
        <th style="width:10%">Quantity*</th>
        <th class="charges-hide-show" style="display:none">Charges</th>
        <th class="charges-hide-show" style="display:none">Charges Account</th>
     
        <th >Price*</th>
        <th class="gst-rate-hide-show" style="display:none">GST Rate</th>
        <th >Amount</th>
        <th style="display:none">status*</th>
        <th style="display:none">ref_no</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        <tr>
          <td>
            <input type="text" list="servicesList" class="service-name" id="service-name" required>
            <input type="hidden"  name="service_name[]" class="service-name-hidden" id="service-name-hidden">
            <input type="hidden"  name="id_services_in_income[]" class="id-services-in-income">
          </td>
          <td><input type="number" onchange="calculation('#editForm')" class="quantity"  name="quantity[]" step="0.01" required></td>
          <td class="charges-hide-show" style="display:none"><input type="number" onchange="calculation('#editForm')" name="charges[]" class="charges"  required></td>
          <td class="charges-hide-show" style="display:none">  <select class="form-control fees_account_id" name="charges_debit_account[]" >
              <option value="0">Select</option>
            <?php foreach($accountsList as $acc): ?>
            <option value="<?php echo $acc['id']; ?>" <?php // echo ($acc['id']==1?'selected':''); ?> ><?php echo $acc['account_name']; ?> </option>
            <?php endforeach; ?>
            </select>
          </td>
          <td><input type="number" id="pwd" class="commission"  onchange="calculation('#editForm')" step="0.01" name="commission[]" required> </td>
          <td class="gst-rate-hide-show" style="display:none"><span class="row-gst-amount" title=""> Rs. 0</span>
            <input type="hidden" class="row-gst-rate" name="gst_rate[]" value="0">
          </td>
          <td><span class="row-total-amount">Rs. 0</span></td>
          <td style="display:none">
            <select name="status[]" class="custom-select1">
            <option value="">Select</option>
            <option value="Pending">Pending</option>
            <option value="Done" selected>Done</option>
          </select>
          </td>
          <td style="display:none"><input type="text" class="refno" id="pwd" placeholder="" name="ref_no[]" ></td>
          <td>
            &nbsp; <a href="#" onclick="addRow(this, '#editForm'); return false"><i class="fas fa-plus"></i></a> &nbsp;
            <a href="#" onclick="deleteRow(this, '#editForm'); return false"><i class="fas fa-trash"></i></a> 
          </td>
        </tr>
      
      </tbody>
    <thead>
      <tr>
        <th colspan="3" class="total-text">Total</th>
        <th class="gst-rate-hide-show total-gst-rate" style="display:none"></th>
        <th><span class="total-bill-amount">Rs. 0</span></th>
        <th></th>
      </tr>
      </thead>

      </table>
      <datalist id="servicesList">
<?php foreach($services as $service): ?>
<option data-value="<?php echo $service['id']; ?>" data-fees_account_id="<?php echo $service['fees_account_id']; ?>" data-igst="<?php echo $service['igst']; ?>" data-hsn="<?php echo $service['hsn']; ?>" data-charges="<?php echo $service['fees']; ?>" data-comm="<?php echo $service['commission']; ?>" value="<?php echo $service['name']; ?>"><?php echo $service['barcode'] ? $service['barcode'] : $service['name']; ?></option>
<?php endforeach; ?>
  </datalist>
  <hr>
</div>


<input type="hidden" name="id">
<div class="form-group">
  <label for="uname">Date (mm/dd/yyyy hh:mm AM/PM)*:</label>
  <input type="datetime-local" class="form-control"  id="addincome-datetime" name="datetime" required>
  <div class="valid-feedback">Valid.</div>
</div>
<div class="form-group">
  <label for="pwd">Customer*:</label>
  <input list="Customerlist" id="customer-id-edit" class="custom-select mb-2" required>
<datalist id="Customerlist">
<?php foreach($customers as $customer): ?>
<option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
<?php endforeach; ?>
  </datalist>
  <input type="hidden" name="customer_id" id="customer-id-edit-hidden">
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>

    <div class="form-group" style="display:none">
<label for="pwd">Total Amount*: </label>
<input type="number" class="form-control bill-amount" id="pwd" step="0.01" placeholder="" name="bill_amount" required>
<div class="valid-feedback">Valid.</div>
<div class="invalid-feedback">Please fill out this field.</div>
</div>


<div class="form-group payment-responsible-by-edit">
  <label for="pwd">Payment Responsible By:</label>
  <input list="Customerlist1" class="custom-select mb-2" id="payment_responsible_by-edit" >
<datalist id="Customerlist1">
<?php foreach($customers as $customer): ?>
<option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
<?php endforeach; ?>
  </datalist>
  <input type="hidden" name="payment_responsible_by" id="payment_responsible_by-edit-hidden">
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>



<div class="form-group" style="display:none">
  <label for="uname">Paper Count(A4)*</label>
  <input type="number" class="form-control"   name="paper_count"  required disabled>
  <div class="valid-feedback">Valid.</div>
</div>

<div class="form-group">
  <label for="">Comments</label>
  <input type="text" class="form-control"   name="comments">
  <div class="valid-feedback">Valid.</div>
</div>



  <div class="ad"><p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary">
  <?php if($ENABLE_PRINT) { ?>  
  <a data-baseurl="<?php echo  base_url('Income/print/'); ?>" href="" target="_blank" class="btn btn-primary print-button">Print</a>
  <?php } ?>
</div>             
      </div>
      </div> 
    </form>

    </div>
    <div id="menu1" class=" tab-pane fade">
    <div class=" payment-history">
      <div class="payment-history-form-div">
      <form onsubmit="addPaymentHistory(); return false" method="post">
    <table class="table1">

      <tr>
        <td >Payment Date* <br> <input type="datetime-local" class="form-control"  name="payment_date" requird></td>
        <td >Amount* <span class="sale-pending-amount"></span> <br><input type="number" class="form-control" step="0.01" name="paid_amount" value="0" required></td>
        <td >Payment Method* <br> 
      
        <select id="assetsList" class="form-control" name="account_id" requird>
  <option value="">Select</option>
<?php foreach($accountsList as $acc): ?>
<option value="<?php echo $acc['id']; ?>" class="<?php echo strtolower($acc['account_name']); ?>"><?php echo $acc['display_name']? $acc['display_name'] : $acc['account_name']; ?></option>
<?php endforeach; ?>
</select>

      </td>
        <td><br><input type="submit"value="Add Payment" class="btn btn-primary"></td>
      </tr>
     
  </table>
</form>

</div>
    <table class="table payment-history-table">
    <thead>
      <tr>
        <th>Payment Date</th>
        <th>Amount</th>
        <th>Account</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
     
    </tbody>
  </table>
  </div>
    </div>
  </div>
</div>

    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->
</div>
</div>



<div class="modal" id="view1">
            <div class="modal-dialog">
              <div class="modal-content">
           <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <td class="id"></td>  
                </tr>
                <tr>
                  <th>Date</th>
                  <td class="datetime"></td>
                </tr>
                <tr>
                  <th>Customer</th>
                  <td class="customer"></td>
                </tr>
                <tr>
                  <th>Bill Amount</tH>
                  <td class="bill_amount"></td>
                </tr>
                <tr style="display:none">
                  <th>Pending Amount</tH>
                  <td class="pending_amount"></td>
                </tr>
                <tr>
                  <th>Payment Status</tH>
                  <td class="payment_status"></td>
                </tr>
                <tr>
                  <th>Account</tH>
                  <td class="account"></td>
                </tr>
                <tr>
                  <th>Payment Responsible By</tH>
                  <td class="payment_responsible_by"></td>
                </tr>
                </thead>
            
                  </table>
<br>
                  <table id="serviceslist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Service Name</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Ref No</th>
                </tr>
               
                </thead>
                <tbody>
</tbody>
                  </table>
                
                  </div>
                  </div>
                  </div>
                  
                  </div>
                  

        <div class="modal" id="add">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Income</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- Modal body -->
      <div class="modal-body">

     

        <h2></h2>
<form onsubmit="addincome(); return false" method="post">
  <div class="form-group">
    <label for="uname">Date :</label>
    <input type="date" class="form-control" id="uname" placeholder="" name="uname" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Customer:</label>
    <input list="browsers" name="browser"class="custom-select mb-2">
  <datalist id="browsers">
    <option value="test1">
    <option value="test2">
    <option value="test3">

    <input type="name" class="form-control" id="pwd" placeholder="" name="pswd" required>
    </datalist>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
 
  <div class=""> <hr>
    <table id=""  class="">
      <thead>
      <tr>
        <th>Serives_id</th>
        <th>Amount</th>
        <th>staus</th>
        <th>ref_no</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td>
        <select name="cars" class="custom-select mb-3">
    <option selected></option>
    <option value="pending">pending</option>
    <option value="paid">paid</option>
  </select>
  </td>
        <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td><i class="fas fa-trash"></i>
          </td>
         </tr>
        <tr>
          <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
          <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
          <td> 
    <select name="cars" class="custom-select mb-3">
    <option selected></option>
    <option value="pending">pending</option>
    <option value="paid">paid</option>
  </select>
  </td>
          <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td><i class="fas fa-trash"></i>

          <td></td>
        </tr>
        </tbody>
        </table>
</div>

      <div class="form-group">
  <label for="pwd">bill_Amount:</label>
  <input type="number" class="form-control" id="pwd" placeholder="" name="pswd" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>
<div class="form-group">
  <label for="pwd">pending_Amount:</label>
  <input type="number" class="form-control" id="pwd" placeholder="" name="pswd" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>
<div class="form-group">
  <label for="pwd">payment_status:</label>
  <select name="cars"   class="custom-select mb-3"> 
    <option selected></option>
    <option value="pending">pending</option>
    <option value="paid">paid</option>
  </select>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>    
</div>
    <div class="ad"><p class="msg" style="color:green;float:left"></p>
    <input type="submit"value="submit" class="btn btn-primary">
    </div>             
        </div>
        </div> 
</form>
        </div>
<?php $this->load->view('footer'); ?>