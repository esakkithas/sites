<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralHelper {

    public $default_tenant_id;
    public $ci;

    public function __construct() {
      $this->ci =& get_instance();
      $this->ci->load->database();
      $this->default_tenant_id = $this->ci->session->userdata('default_tenant_id'); 
    }
    function getSettings($setting_key){
        $result = null;
        $settings_row = $this->ci->db->where('tenant_id', $this->default_tenant_id)->where('setting_key', $setting_key)->get('s_tenant_settings')->row();
        if(isset($settings_row->id)){
           $result = $settings_row->setting_value;
        }
        return $result;
     }
     function getInvoiceData($income_id){
      $result = array();

      $this->ci->db->from('s_tenants');
      $this->ci->db->where('id', $this->default_tenant_id);
      $query=$this->ci->db->get();
      $result['tenant'] = $query->row();

		$ci_sql = 'SELECT *, s_customers.name as customer_name, s_customers.id as customer_id, s_income.id as income_id   FROM s_income
		INNER JOIN s_customers ON s_customers.id=s_income.customer_id
		WHERE s_income.id = "'.$income_id.' and s_income.tenant_id='.$this->default_tenant_id.' and s_customers.tenant_id='.$this->default_tenant_id.'" 
		';
		$ci_query = $this->ci->db->query($ci_sql);
		$result['invoice']=  $ci_query->row();

      $services_in_income_sql = 'SELECT *, s_products_and_services.name as service_name, s_products_and_services.display_name as display_name, s_services_in_income.id as id_services_in_income, s_services_in_income.commission as commission_new, s_services_in_income.charges_debit_account_id as charges_debit_account_id FROM s_services_in_income
		INNER JOIN s_income ON s_services_in_income.income_id=s_income.id
		INNER JOIN s_products_and_services ON s_services_in_income.service_id=s_products_and_services.id
		WHERE s_income.id ="'.$income_id.'" and s_services_in_income.tenant_id='.$this->default_tenant_id.' and s_services_in_income.status=1 and s_income.tenant_id='.$this->default_tenant_id.' and s_products_and_services.tenant_id='.$this->default_tenant_id.'
		';
		$services_in_income_query = $this->ci->db->query($services_in_income_sql);
		$result['services']=  $services_in_income_query->result_array();

      $this->ci->db->from('s_tenant_bank_accounts');
      $this->ci->db->where('tenant_id', $this->default_tenant_id)->where('status', 1);
      $query=$this->ci->db->get();
      $result['tenant_bank_account'] = $query->row();

      return $result;
     }
}