<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $default_tenant_id;
	public function __construct()  {
		parent:: __construct();
		
		// $this->load->helper('general_helper');
		// $test = getTenant(); echo $test;die;
		// $this->load->model('tenant_model');
		// $this->tenant_model->get_user_tenants();
		$this->load->model('reg_new_model');
		$this->load->library('pagination');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$this->default_tenant_id = $this->session->userdata('default_tenant_id'); 
		if(empty($this->default_tenant_id)){
			echo "You do not have access to view this page."; die;
		}
	}
	public function index()
	{
		$data = array();
		// $sql="SELECT DATE_FORMAT(datetime,'%Y-%m-%d') as transaction_date FROM s_expenses where status=1 UNION SELECT DATE_FORMAT(datetime,'%Y-%m-%d') as transaction_date FROM s_income where status=1 and tenant_id='$this->default_tenant_id' order by transaction_date desc";    
		$sql ="SELECT distinct DATE_FORMAT(datetime,'%Y-%m-%d') as transaction_date FROM s_income where status=1 and tenant_id='$this->default_tenant_id' order by transaction_date desc ";    
		$pagination_query = $this->db->query($sql);
		if(isset($_GET['per_page'])){
			$sql .="limit ".$_GET['per_page'].", 10";    
		}else{
			$sql .="limit 0, 10";    
		}
		
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['list'] =  $query->result_array();
		$data['t'] =  $this;
		$data['accounts'] = $this->getAccountsList();
		// $data['account_balance_details'] = array();
		// foreach($data['accounts'] as $account){
		// 	$data['account_balance_details'][$account['id']] = 0;
		// }
		$config['page_query_string'] = TRUE;
		$config['base_url'] = base_url(). '/Dashboard';
		$config['total_rows'] = $pagination_query->num_rows();
		$config['per_page'] = 10;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$this->load->view('dashboard',$data);
	}
	public function getAccountBalance($accounts, $date){
		$account_balance_details = array();
		$account_balance_details["total"] = 0;
		foreach($accounts as $account){
			$sql="SELECT sum(amount) as amount FROM s_transactions WHERE DATE_FORMAT(datetime,'%Y-%m-%d') <= '".$date."' and account_id='".$account['id']."' and status=1 and tenant_id=".$this->default_tenant_id;    
			// echo $sql;die;
			$query = $this->db->query($sql);
			$data =  $query->row();
			$account_balance_details[$account['id']] = ceil(isset($data->amount) ? $data->amount : 0);
			$account_balance_details["total"] += $account_balance_details[$account['id']];
		}
		return $account_balance_details;
	}
	public function getPendingAmount($date){
		$pending_amount = 0;
		$sql='SELECT max(s_income.id) as income_id, max(s_income.bill_amount) as bill_amount, sum(s_transactions.amount) as transaction_amount FROM s_transactions RIGHT JOIN s_income ON s_transactions.income_id=s_income.id and s_transactions.datetime <= "'.$date.' 23:59:59" and s_transactions.amount>0 and s_transactions.status=1 and s_transactions.tenant_id='.$this->default_tenant_id.' WHERE  s_income.status=1 and s_income.tenant_id='.$this->default_tenant_id.' and s_income.datetime <= "'.$date.' 23:59:59" group by s_income.id';  
		// echo $sql;die;
		$query = $this->db->query($sql);
		$results =  $query->result_array();
		foreach($results as $row){
			if($row['bill_amount'] > (isset($row['transaction_amount'])?$row['transaction_amount']:0) ){
				$pending_amount += ceil($row['bill_amount']) - ceil($row['transaction_amount']);
			}
		}
		return $pending_amount;
	}
	public function getPendingAmountTransactions(){
		$input = $this->input->post();
		$pending_amount = 0;
		$sql='SELECT s_customers.name as customer_name, s_customers.id as customer_id, max(s_income.datetime) as income_datetime, max(s_income.id) as income_id, max(s_income.bill_amount) as bill_amount, sum(CEIL(s_transactions.amount)) as transaction_amount FROM s_transactions 
		RIGHT JOIN s_income ON s_transactions.income_id=s_income.id and s_transactions.datetime <= "'.$input['transactionDate'].' 23:59:59" and s_transactions.amount>0 and s_transactions.status=1 and s_transactions.tenant_id='.$this->default_tenant_id.' 
		RIGHT JOIN s_customers ON s_income.customer_id=s_customers.id 
		WHERE  s_income.status=1 and s_income.tenant_id='.$this->default_tenant_id.' and s_income.datetime <= "'.$input['transactionDate'].' 23:59:59" group by s_income.id order by s_customers.id, income_datetime';  
		// echo $sql;die;

		$query = $this->db->query($sql);
		// $results =  $query->result_array();
		// foreach($results as $row){
		// 	if($row['bill_amount'] > (isset($row['transaction_amount'])?$row['transaction_amount']:0) ){
		// 		$pending_amount += $row['bill_amount'] - $row['transaction_amount'];
		// 	}
		// }
		// echo $pending_amount;die;


		$list =  $query->result_array();


		$results = array();
		foreach($list as $l){
			$pending_amount = $l['bill_amount'] - (isset($l['transaction_amount'])?$l['transaction_amount']:0);
			if($pending_amount > 0){
				$results[$l['customer_id']][] = $l;
			}
		}

		$data['list'] = array();
		$data['total_pending_amount'] = 0;
		$data['customer_pending_amount'] = array();
		foreach($results as $customer_n => $income_list){
			$count = 0;
			$data['customer_pending_amount'][$customer_n] = 0;
			foreach($income_list as $l){
				$pending_amount = $l['bill_amount'] - ceil(isset($l['transaction_amount'])?$l['transaction_amount']:0);
				if($pending_amount > 0){
					$count++;
					$data['customer_pending_amount'][$l['customer_id']] += (int)$pending_amount;
					$data['total_pending_amount'] += ceil($pending_amount);
					$l['rowspan'] = $count==1?count($results[$customer_n]):0;
					$l['pending_amount'] = $pending_amount;
					$data['list'][] = $l;
				}
			}
		}

		$this->load->view('dashboard_view_pending_transactions',$data);
	}
	public function getIncomeTotal($date){
		#$sql = "SELECT sum(bill_amount) as total,  DATE_FORMAT(datetime,'%Y-%m-%d') as d FROM s_income where  DATE_FORMAT(datetime,'%Y-%m-%d')='".$date."'  and status=1 group by d";
		// $sql = "SELECT sum(amount) as total,  DATE_FORMAT(datetime,'%Y-%m-%d') as d FROM s_transactions where  DATE_FORMAT(datetime,'%Y-%m-%d')='".$date."' and income_id is not null  and status=1 group by d";
		// $sql = "SELECT sum(quantity * commission) as total,  DATE_FORMAT(datetime,'%Y-%m-%d') as d FROM s_services_in_income where  DATE_FORMAT(datetime,'%Y-%m-%d')='".$date."' and status=1";
		$sql = "SELECT sum(CEILING(quantity * commission)) as total,  DATE_FORMAT(datetime,'%Y-%m-%d') as d  FROM s_services_in_income
				INNER JOIN s_income ON s_services_in_income.income_id=s_income.id
				Where DATE_FORMAT(s_income.datetime,'%Y-%m-%d')='".$date."' and s_services_in_income.status=1 and s_income.status=1 and s_services_in_income.tenant_id=".$this->default_tenant_id." and s_income.tenant_id=".$this->default_tenant_id." group by d
				";
				// echo $sql;die;

		$query = $this->db->query($sql);
		$data=  $query->row();
		
		return (isset($data) ? $data->total : "0");
	}
	public function getIncomeDetails(){
		$input = $this->input->post();
		$data = array();
		$sql = 'SELECT *, s_services_in_income.commission as sii_commission, s_products_and_services.name as service_name, s_customers.name as customer_name  FROM s_services_in_income
				INNER JOIN s_income ON s_services_in_income.income_id=s_income.id
				INNER JOIN s_products_and_services ON s_services_in_income.service_id=s_products_and_services.id
				INNER JOIN s_customers ON s_income.customer_id=s_customers.id 
				WHERE DATE_FORMAT(s_income.datetime,"%Y-%m-%d")="'.$input['transactionDate'].'" and s_income.status=1 
				and s_services_in_income.tenant_id='.$this->default_tenant_id.' and s_services_in_income.status=1 and s_income.tenant_id='.$this->default_tenant_id.' 
				and s_products_and_services.tenant_id='.$this->default_tenant_id.'  and s_customers.tenant_id='.$this->default_tenant_id.' order by s_income.datetime asc 
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['list']=  $query->result_array();
		$this->load->view('dashboard_view_income',$data);
	}
	public function getTransactionDetails(){
		$input = $this->input->post();
		$data = array();
		$sql = 'SELECT s_transactions.id as transaction_id,  s_transactions.* FROM s_transactions
				LEFT JOIN s_income ON s_transactions.income_id=s_income.id
				WHERE DATE_FORMAT(s_transactions.datetime,"%Y-%m-%d")="'.$input['transactionDate'].'" and s_transactions.account_id="'.$input['accountId'].'" and (s_income.status=1 OR s_income.status is null) 
				and s_transactions.tenant_id='.$this->default_tenant_id.'  and (s_income.tenant_id='.$this->default_tenant_id.' or s_income.tenant_id is null) and s_transactions.status=1
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['list']=  $query->result_array();
		// print_r($data['list']);die;
		$this->load->view('dashboard_view_transactions',$data);
	}
	public function getExpenseDetails(){
		$input = $this->input->post();
		$data = array();
		$sql = 'SELECT  *, s_products_and_services.name as product_name from s_expenses
				LEFT JOIN s_products_and_services ON s_expenses.product_id=s_products_and_services.id  and s_products_and_services.tenant_id='.$this->default_tenant_id.' 
				WHERE DATE_FORMAT(datetime,"%Y-%m-%d")="'.$input['transactionDate'].'" and s_expenses.status=1
				and s_expenses.tenant_id='.$this->default_tenant_id.'
				';
		// echo $sql;die;
		$data['list'] = $this->db->query($sql)->result_array();
		$this->load->view('dashboard_view_expense',$data);
	}
	public function getExpenseTotal($date){
		$sql = "SELECT sum(amount) as total,  DATE_FORMAT(datetime,'%Y-%m-%d') as d FROM s_expenses where DATE_FORMAT(datetime,'%Y-%m-%d')='".$date."' and status=1 and tenant_id=".$this->default_tenant_id."  group by d";
		$query = $this->db->query($sql);
		$data=  $query->row();
		
		return (isset($data) ? $data->total : "0");
	}
	public function getAccountsList(){
		$sql = "SELECT * FROM s_accounts_list where tenant_id=".$this->default_tenant_id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function updateGoogleSheet(){

		$results = array();
		$tenant = $this->db->where('id', $this->default_tenant_id)->get('s_tenants')->row_array();
		$results['spreadsheet_id'] = $tenant['spreadsheet_id'];

		$customers_sql = 'SELECT c1.*, c2.name as referred_by_name from s_customers c1 
		LEFT JOIN s_customers c2 ON c1.referred_by=c2.id 
		where c1.status in (1,0) and c1.tenant_id='. $this->default_tenant_id;
		$customer_query = $this->db->query($customers_sql);
		$customer_results =  $customer_query->result_array();
		$results['customers'] = array();
		$results['customers'][] = ['Id', 'Date*', 'Customer Id*', 'Name*', 'Mobile*', 'Address*', 'Referred By', 'Notification', 'Borrow Limit'];
		foreach($customer_results as $key=>$customer){
			$results['customers'][] = array($customer['id'],
			$customer['created_at'],$customer['customer_id'],$customer['name'],$customer['mobile'],$customer['address'],isset($customer['referred_by_name'])?$customer['referred_by_name']:'', $customer['notification'], $customer['borrow_limit']);
		
		}
		// print_r($results['customers']);die;

		$expense_sql = 'SELECT  s_expenses.*, s_products_and_services.name as product_name, s_master_expense.name as expense_details from s_expenses
				LEFT JOIN s_products_and_services ON s_expenses.product_id=s_products_and_services.id 
				LEFT JOIN s_master_expense ON s_expenses.id_master_expense=s_master_expense.id
				WHERE s_expenses.status=1 and s_expenses.tenant_id='.$this->default_tenant_id.'
				';
		$expense_query = $this->db->query($expense_sql);
		$expense_results =  $expense_query->result_array();
		$results['expenses'] = array();
		$results['expenses'][] = ['Id', 'Date*', 'Expense Type*', 'Purchase Entry', 'Shop Expense', 'Stock', 'Amount*', 'Comments'];
		foreach($expense_results as $key=>$expense_result){
			$results['expenses'][] = array($expense_result['id'],$expense_result['datetime'],isset($expense_result['expense_type']) ? ($expense_result['expense_type']=='1'? 'Purchase Entry' : 'Shop Expense') : '',
			isset($expense_result['product_name']) ? $expense_result['product_name']: '', isset($expense_result['expense_details'])?$expense_result['expense_details']:'' ,$expense_result['count'],$expense_result['amount'], $expense_result['additional_info']);
		}


		$assets_sql = 'SELECT * from s_assets where status in (1,0) and tenant_id='. $this->default_tenant_id;
		$asset_results = $this->db->query($assets_sql)->result_array();
		$results['assets'] = array();
		$results['assets'][] = ['Id', 'Name', 'Quantity'];
		foreach($asset_results as $key=>$asset_result){
			$results['assets'][] = array($asset_result['id'],
			$asset_result['name'],$asset_result['stock']);
		}



		$services_sql = 'SELECT  s_products_and_services.*, s_categories.name as category_name from s_products_and_services
				LEFT JOIN s_categories ON s_products_and_services.category_id=s_categories.id 
				WHERE s_products_and_services.status=1 and s_products_and_services.tenant_id='.$this->default_tenant_id.'
				';
		$service_results =  $asset_results = $this->db->query($services_sql)->result_array();
		$results['services'] = array();
		$results['services'][] = ['Id', 'Service/Product*', 'Charges*', 'Charges Account', 'Price*', 'Total*', 'Category*'];
		foreach($service_results as $key=>$service_result){
			$results['services'][] = array($service_result['id'],$service_result['name'],$service_result['fees'],
			isset($service_result['fees_account_id'])?$service_result['fees_account_id']:'',$service_result['commission'],strval($service_result['fees'] + $service_result['commission']), isset($service_result['category_name'])?$service_result['category_name']:'',
			);
		}



		$sql="SELECT DATE_FORMAT(datetime,'%Y-%m-%d') as transaction_date FROM s_expenses where status=1 UNION SELECT DATE_FORMAT(datetime,'%Y-%m-%d') as transaction_date FROM s_income where status=1 and tenant_id='$this->default_tenant_id' order by transaction_date desc";    
		$master_results = $this->db->query($sql)->result_array();
		$results['master'] = array();
		$accounts = $this->getAccountsList();
		$headers = array('Date*', 'Sale*', 'Expense*');
		foreach($accounts as $account){
			$headers[] = $account['account_name'].'*';
		}
		$master_headers = array_merge($headers, ['Pending*', 'Total*', 'Calculation Mistake*', 'Calculation By*', 'Comments', 'Print Count (Machine)*', 'Print Count (Actual)*', 'Lost Customers Count*']);
		foreach($master_results as $key=>$master_result){
			$temp_rows = array();
			$temp_rows[] = $master_result['transaction_date'];
			$temp_rows[] = number_format((float)$this->getIncomeTotal($master_result['transaction_date']), 2, '.', '');
			$temp_rows[] = number_format((float)$this->getExpenseTotal($master_result['transaction_date']), 2, '.', '');
			$account_balance_details = $this->getAccountBalance($accounts, $master_result['transaction_date']);
			foreach($accounts as $account){
				$temp_rows[] = number_format((float)$account_balance_details[$account['id']], 2, '.', '');
			}
			$temp_rows[] = number_format((float)$this->getPendingAmount($master_result['transaction_date']), 2, '.', '');
			$temp_rows[] = number_format((float)$account_balance_details['total'] + $this->getPendingAmount($master_result['transaction_date']), 2, '.', '');
			$results['master'][] = $temp_rows;
		}
		$results['master'] = array_reverse($results['master']);
		array_unshift($results['master'] , $master_headers);



		$income_sql = 'SELECT (select sum(amount) from s_transactions where income_id=s_income.id and amount > 0) as paid_amount, s_services_in_income.*, s_users.name as created_by_name,c2.name as payment_responsible_by_name,s_customers.name as customer_name, s_income.datetime as income_datetime, s_income.bill_amount, s_products_and_services.name as service_name, s_income.payment_status as payment_status, s_income.payment_responsible_by, s_income.comments from s_services_in_income
				INNER JOIN s_income ON s_services_in_income.income_id=s_income.id 
				INNER JOIN s_products_and_services ON s_services_in_income.service_id=s_products_and_services.id 
				LEFT JOIN s_customers ON s_income.customer_id=s_customers.id 
				LEFT JOIN s_customers c2 ON s_income.payment_responsible_by=c2.id 
				LEFT JOIN s_users ON s_income.created_by=s_users.id 
				WHERE s_income.status=1 and s_income.tenant_id='.$this->default_tenant_id.'
				';
		$income_results =  $asset_results = $this->db->query($income_sql)->result_array();
		$results['sale'] = array();
		$results['sale'][] = ['Id', 'Date*', 'Customer*', 'Item Details*', 'Quantity*', 'Charges*', 'Price', 'Total Price*','Total Amount*', 'Payment Status*', 'Pending Amount*', 'Payment Responsible By', 'Computer Operator*', 'Comments'];
		$prev_income_id = 0;
		foreach($income_results as $key=>$income_result){
			$payment_status = '';
			if($income_result['payment_status']=='0'){
				$payment_status = 'Pending';
			}else if($income_result['payment_status']=='1'){
				$payment_status = 'Paid';
			}else if($income_result['payment_status']=='2'){
				$payment_status = 'Partial Payment';
			}
			$pending_amount = ($income_result['income_id'] != $prev_income_id ? (ceil($income_result['bill_amount'] - $income_result['paid_amount'])) : 0);
			$results['sale'][] = array(
				$income_result['income_id'],
				$income_result['income_datetime'],
				isset($income_result['customer_name']) ? $income_result['customer_name'] : 'NA',
				$income_result['service_name'],
				$income_result['quantity'],
				$income_result['charges'],
				$income_result['commission'],
				ceil($income_result['quantity'] * $income_result['commission']),
				ceil($income_result['quantity'] * ($income_result['charges'] + $income_result['commission'])),
				$payment_status,
				($pending_amount <= 1 ? 0 : $pending_amount),
				isset($income_result['payment_responsible_by_name']) ? $income_result['payment_responsible_by_name']: '',
				$income_result['created_by_name'],
				$income_result['comments'],
		);
		$prev_income_id = $income_result['income_id'];
		}

		$myfile = fopen("../../google/data.json", "w") or die("Unable to open file!");
		fwrite($myfile, json_encode($results));
		fclose($myfile);

		// print_r(json_encode($results['sale']));die;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERPWD, "srina:5ilF125+-8>");
		curl_setopt($ch, CURLOPT_URL,"https://google.nellaicity.com/update_sheet.php");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 
		         http_build_query(array('data'=>'data')));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		curl_close($ch);
		print_r($server_output);die;
	
		}
}