<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $default_tenant_id;
	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$this->default_tenant_id = $this->session->userdata('default_tenant_id'); 
		if(empty($this->default_tenant_id)){
			echo "You do not have access to view this page."; die;
		}
		$this->load->library('GeneralHelper');
	}
	public function index()
	{
		$data = array();
		$where = '(id<=3 or tenant_id = '.$this->default_tenant_id.')';
		$data['categories_list'] = $this->db->where_in('status', [1,0])->where($where)->get('s_categories')->result_array();
		$data['accountsList'] = $this->db->where('tenant_id', $this->default_tenant_id)->get('s_accounts_list')->result_array();
		
		$data['tenant'] = $this->db->where('id', $this->default_tenant_id)->get('s_tenants')->row_array();
		$data['ENABLE_GST'] = $this->generalhelper->getSettings('ENABLE_GST');
		$data['ENABLE_CHARGES'] = $this->generalhelper->getSettings('ENABLE_CHARGES');

		$this->load->view('services',$data);
	}
	public function list1()
	{
		$data = array();
		// $data['data'] = $this->db->where_in('status', [1,0])->get('s_products_and_services')->result_array();
		$sql = 'SELECT  *, s_categories.name as category_name, s_categories.slug as category_slug,  s_products_and_services.name as service_name,s_products_and_services.id as service_id from s_products_and_services
				LEFT JOIN s_categories ON s_products_and_services.category_id=s_categories.id 
				WHERE s_products_and_services.status in (1,0) and s_products_and_services.tenant_id='.$this->default_tenant_id.' order by s_products_and_services.category_id DESC
				';
		$query = $this->db->query($sql);
		$data['data']=  $query->result_array();

		return print_r(json_encode($data));
	}
	public function delete()
	{
		
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_products_and_services', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();

		$sql = 'SELECT s_products_and_services.*, s_categories.slug as category_slug, s_categories.parent_id as category_parent_id from s_products_and_services 
		LEFT JOIN s_categories ON s_products_and_services.category_id=s_categories.id 
		where s_products_and_services.status=1 and s_products_and_services.id='.$input['id'].' and s_products_and_services.tenant_id='.$this->default_tenant_id;

		$data['data'] = $this->db->query($sql)->row_array();

		// $data['root_category_slug'] = '';
		// if(isset($data['data']['category_slug']) && $data['data']['category_slug'] == 'services'){
		// 	$data['root_category_slug'] = 'services';
		// }else if(isset($data['data']['category_parent_id']) && !empty($data['data']['category_parent_id'])){
		// 	$sql = 'SELECT * from s_categories where status=1 and id='.$data['data']['category_parent_id'];
		// 	$parent_category = $this->db->query($sql)->row_array();
		// 	if(isset($parent_category['slug'])){
		// 		$data['root_category_slug'] = $parent_category['slug'];
		// 	}
		// }

		$data['stock'] = $this->stock($input['id']);
		return print_r(json_encode($data));
	}
	
	public function add()
	{
		
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$input = $this->input->post();
		$input['created_by'] = $this->session->userdata('userid'); 
		$input['tenant_id'] = $this->default_tenant_id;
		$this->db->insert('s_products_and_services',$input);
		return print_r(json_encode($input));
	}
	public function edit()
	{
		
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}	
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 

		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_products_and_services',$input);
		return print_r(json_encode($input));
	}
	public function stock($product_id)
	{
		$sql = 'SELECT sum(count) as purchased_quantity FROM `s_expenses` WHERE product_id ='.$product_id.' and status=1 and tenant_id='.$this->default_tenant_id;
		$query = $this->db->query($sql);
		$purchase =  $query->row();

		$sql = 'SELECT sum(s_services_in_income.quantity) as sold_quantity from s_services_in_income INNER JOIN s_products_and_services ON s_services_in_income.service_id=s_products_and_services.id where s_services_in_income.service_id='.$product_id.' and s_services_in_income.status=1 and s_services_in_income.tenant_id='.$this->default_tenant_id;
		$query = $this->db->query($sql);
		$sold =  $query->row();
		
		return (empty($purchase->purchased_quantity) ? 0 :  $purchase->purchased_quantity) -  (empty($sold->sold_quantity) ? 0 : $sold->sold_quantity);
	}
	
}