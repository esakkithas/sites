<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assets extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $default_tenant_id;
	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$this->default_tenant_id = $this->session->userdata('default_tenant_id'); 
		if(empty($this->default_tenant_id)){
			echo "You do not have access to view this page."; die;
		}
		
	}
	public function index()
	{
		$data = array();
 
		$this->load->view('assets',$data);
	}
	public function list1()
	{
		$data = array();
		$data['data'] = $this->db->where_in('status', [1,0])->where('is_hidden', 0)->where('tenant_id', $this->default_tenant_id)->get('s_assets')->result_array();
		return print_r(json_encode($data));
	}
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_assets', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$data['data'] =	$this->db->get('s_assets')->row_array();
		return print_r(json_encode($data));
	}
	
	public function add()
	{
		$input = $this->input->post();
		$input['created_by'] = $this->session->userdata('userid'); 
		$input['tenant_id'] = $this->default_tenant_id;
		$this->db->insert('s_assets',$input);
		return print_r(json_encode($input));
	}
	public function edit()
	{	
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_assets',$input);
		return print_r(json_encode($input));
	}

}