<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $default_tenant_id;
	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$this->default_tenant_id = $this->session->userdata('default_tenant_id'); 
		if(empty($this->default_tenant_id)){
			echo "You do not have access to view this page."; die;
		}
	}
	public function index()
	{
		$product_category = $this->db->where_in('status', [1,0])->where('slug', 'products')->get('s_categories')->row();
		$data = array();
		$data['productsList'] = array();
		if(isset($product_category->id)){
			$data['productsList'] = $this->db->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->where('category_id', $product_category->id)->get('s_products_and_services')->result_array();
		}
		$data['assetsList'] = $this->db->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->get('s_assets')->result_array();
		$data['accountsList'] = $this->db->where('tenant_id', $this->default_tenant_id)->get('s_accounts_list')->result_array();
		$data['masterExpensesList'] = $this->db->where('tenant_id', $this->default_tenant_id)->get('s_master_expense')->result_array();
		$this->load->view('expense',$data);
	}
	public function list1()
	{
		$data = array();
		// $sql = 'SELECT  *, s_products_and_services.name as asset_name, s_expenses.id as expense_id from s_expenses
		// 		INNER JOIN s_products_and_services ON s_expenses.service_or_product_id=s_products_and_services.id 
		// 		WHERE s_expenses.status in (1,0) and s_expenses.tenant_id = '.$this->default_tenant_id.' and s_products_and_services.tenant_id = '.$this->default_tenant_id.'
		// 		';
		
		$sql = 'SELECT  *, s_products_and_services.name as asset_name, s_expenses.id as expense_id from s_expenses
				LEFT JOIN s_products_and_services ON s_expenses.product_id=s_products_and_services.id and s_products_and_services.tenant_id = '.$this->default_tenant_id.'
				LEFT JOIN s_assets ON s_expenses.asset_id=s_assets.id and s_assets.tenant_id = '.$this->default_tenant_id.'
				LEFT JOIN s_master_expense ON s_expenses.id_master_expense=s_master_expense.id and s_master_expense.tenant_id = '.$this->default_tenant_id.'
				WHERE s_expenses.status in (1,0) and s_expenses.tenant_id = '.$this->default_tenant_id.'
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['data']=  $query->result_array();
		return print_r(json_encode($data));
	}
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$this->db->update('s_expenses', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$this->db->where('expense_id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_transactions', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();
		// $this->db->where('id',$input['id']);
		// $data['data'] =	$this->db->get('s_expenses')->row_array();
		$sql = 'SELECT  *, s_products_and_services.name as asset_name, s_expenses.id as expense_id,s_master_expense.name as master_expense_name from s_expenses
		LEFT JOIN s_products_and_services ON s_expenses.product_id=s_products_and_services.id and s_products_and_services.tenant_id = '.$this->default_tenant_id.'
				LEFT JOIN s_assets ON s_expenses.asset_id=s_assets.id and s_assets.tenant_id = '.$this->default_tenant_id.'
				LEFT JOIN s_master_expense ON s_expenses.id_master_expense=s_master_expense.id and s_master_expense.tenant_id = '.$this->default_tenant_id.'
		WHERE s_expenses.id = "'.$input['id'].' and s_expenses.tenant_id = '.$this->default_tenant_id.'"
		';
// echo $sql;die;
$query = $this->db->query($sql);
$data['data']=  $query->row();
		return print_r(json_encode($data));
	}
	public function add()
	{
		$input = $this->input->post();
		$input['created_by'] = $this->session->userdata('userid'); 
		$input['tenant_id'] = $this->default_tenant_id;
		$account_id = $input['account_id'];
		unset($input['account_id']);

		if($input['expense_type']==0){
			unset($input['product_id']);
			$input['count'] = 0;
			unset($input['asset_id']);
		}else if($input['expense_type']==1){
			unset($input['id_master_expense']);
		}
		$this->db->insert('s_expenses',$input);
		$insert_id = $this->db->insert_id();
		// echo $insert_id;
		// print_r($input);die;

		$transactions =array();
		$transactions['created_by'] = $this->session->userdata('userid'); 
		$transactions['tenant_id'] = $this->default_tenant_id;
		$transactions['expense_id'] = $insert_id;
		$transactions['datetime'] = $input['datetime'];
		$transactions['account_id'] = $account_id;
		$transactions['amount'] = -1 * abs($input['amount']);
		$this->db->insert('s_transactions',$transactions);


		return print_r(json_encode($input));
	}
	public function edit()
	{	
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_expenses',$input);
		return print_r(json_encode($input));
	}

}