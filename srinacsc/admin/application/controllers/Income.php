<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $default_tenant_id;
	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$this->default_tenant_id = $this->session->userdata('default_tenant_id'); 
		if(empty($this->default_tenant_id)){
			echo "You do not have access to view this page."; die;
		}
		$this->load->library('GeneralHelper');

	}
	public function index()
	{
		$data = array();
		$data['customers'] = $this->db->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->get('s_customers')->result_array();
		$data['services'] = $this->db->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->get('s_products_and_services')->result_array();
		$data['accountsList'] = $this->db->where('tenant_id', $this->default_tenant_id)->order_by("sort_order", "asc")->get('s_accounts_list')->result_array();
		$data['ENABLE_GST'] = $this->generalhelper->getSettings('ENABLE_GST');
		$data['ENABLE_PRINT'] = $this->generalhelper->getSettings('ENABLE_PRINT');
		$this->load->view('income',$data);
	}
	public function list1()
	{
		$input = $this->input->get();
		$data = array();
		// $data['data'] = $this->db->where_in('status', [1,0])->get('s_income')->result_array();
		$sql = 'SELECT *, s_customers.name as customer_name,s_income.id as income_id  FROM s_income
				INNER JOIN s_customers ON s_customers.id=s_income.customer_id
				WHERE s_income.status in (1,0)  and s_income.tenant_id='.$this->default_tenant_id.' and s_customers.tenant_id='.$this->default_tenant_id.'
				';
		if(isset($input['from_date']) && $input['from_date']){
			$sql .= ' and s_income.datetime>="'.$input['from_date'].' 00:00:00" ';
		}
		if(isset($input['from_date']) && $input['from_date']){
			$sql .= ' and s_income.datetime<="'.$input['to_date'].' 23:59:59" ';
		}
		$query = $this->db->query($sql);
		$data['data']=  $query->result_array();
		return print_r(json_encode($data));
	}
	
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		
		// $update_data = array();
		// $user_id = $this->session->userdata('userid');
		// if($user_id <= 2){
		// 	$update_data = array('status'=>-1, 'deleted_by'=>$user_id);
		// }else{
		// 	$update_data = array('status'=>0, 'modified_by'=>$user_id);

		// }
		

		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_income', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		
		$this->db->where('income_id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_transactions', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		
		$this->db->where('income_id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_services_in_income', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();

		$sql = 'SELECT *, s_customers.name as customer_name, s_customers.id as customer_id, s_income.id as income_id   FROM s_income
		INNER JOIN s_customers ON s_customers.id=s_income.customer_id
		WHERE s_income.id = "'.$input['id'].' and s_income.tenant_id='.$this->default_tenant_id.' and s_customers.tenant_id='.$this->default_tenant_id.'" 
		';
		$query = $this->db->query($sql);
		$data['data']=  $query->row_array();

		$sql1 = 'SELECT *, s_products_and_services.name as service_name, s_services_in_income.id as id_services_in_income, s_services_in_income.commission as commission_new, s_services_in_income.charges_debit_account_id as charges_debit_account_id FROM s_services_in_income
		INNER JOIN s_income ON s_services_in_income.income_id=s_income.id
		INNER JOIN s_products_and_services ON s_services_in_income.service_id=s_products_and_services.id
		WHERE s_income.id ="'.$input['id'].'" and s_services_in_income.tenant_id='.$this->default_tenant_id.' and s_services_in_income.status=1 and s_income.tenant_id='.$this->default_tenant_id.' and s_products_and_services.tenant_id='.$this->default_tenant_id.'
		';
		// echo $sql1;die;
		$query1 = $this->db->query($sql1);
		$data['serviceslist']=  $query1->result_array();

		// $sql3 = 'SELECT *  FROM s_transactions WHERE income_id = "'.$input['id'].'" and amount > 0 and account_id=7';
		// $query3 = $this->db->query($sql3);
		// $data['pending']=  $query3->row_array();

		// $sql4 = 'SELECT *  FROM s_transactions WHERE income_id = "'.$input['id'].'" and amount > 0 and account_id!=7';
		// $query4 = $this->db->query($sql4);
		// $data['paid']=  $query4->row_array();

		$sql5 = 'SELECT sum(amount) as total_amount  FROM s_transactions WHERE income_id = "'.$input['id'].'" and amount > 0 and status=1 and tenant_id='.$this->default_tenant_id;

		$query5 = $this->db->query($sql5);
		$result5 = $query5->row_array();
		$data['paid_amount']=  isset($result5['total_amount']) ? $result5['total_amount'] : 0;



		$sql6 = 'SELECT *,s_transactions.id as transaction_id from s_transactions INNER JOIN s_accounts_list ON s_transactions.account_id=s_accounts_list.id where income_id="'.$input['id'].'" and s_transactions.tenant_id='.$this->default_tenant_id.' and status=1 and amount > 0';

		$query6 = $this->db->query($sql6);
		$data['transactions']=  $query6->result_array();


		return print_r(json_encode($data));
	}
	public function add()
	{
		$input = $this->input->post();

		$insert_data = array();
		$insert_data['tenant_id'] = $this->default_tenant_id;
		$insert_data['datetime'] = $input['datetime'];
		$insert_data['gst_bill'] = $input['gst_bill'];
		$insert_data['customer_id'] = $input['customer_id'];
		$insert_data['bill_amount'] = $input['bill_amount'];
		$insert_data['payment_status'] = $input['payment_status'];
		$insert_data['payment_responsible_by'] = $input['payment_responsible_by'];
		$insert_data['paper_count'] = $input['paper_count'];
		$insert_data['comments'] = $input['comments'];
		$insert_data['created_by'] = $this->session->userdata('userid'); 
		$this->db->insert('s_income',$insert_data);
		$income_id = $this->db->insert_id();

		foreach($input['service_name'] as $key=>$value){
			$services_in_income = array();
			$services_in_income['tenant_id'] = $this->default_tenant_id;
			$services_in_income['income_id'] = $income_id;
			$services_in_income['service_id'] = $input['service_name'][$key];
			$services_in_income['ref_no'] = $input['ref_no'][$key];
			$services_in_income['work_status'] = $input['status'][$key];
			$services_in_income['quantity'] = $input['quantity'][$key];
			$services_in_income['charges'] = $input['charges'][$key];
			if(isset($input['charges_debit_account'][$key]) && $input['charges_debit_account'][$key]>0){

				$services_in_income['charges_debit_account_id'] = $input['charges_debit_account'][$key];
			}
			$services_in_income['commission'] = $input['commission'][$key];
			$services_in_income['gst_rate'] = $input['gst_rate'][$key];
			$services_in_income['total_amount'] = $input['charges'][$key] + $input['commission'][$key];
			if($input['gst_bill'] > 0){
				$services_in_income['gst_value'] = $services_in_income['gst_rate'] / 100 * $services_in_income['commission'];
				$services_in_income['total_amount'] += $services_in_income['gst_value'];
			}
			$services_in_income['total_amount'] = $services_in_income['total_amount'] *  $input['quantity'][$key];
			$services_in_income['created_by'] = $this->session->userdata('userid'); 
			$this->db->insert('s_services_in_income',$services_in_income);
			$id_services_in_income = $this->db->insert_id();
			if($input['charges'][$key] > 0){
				$transactions =array();
				$transactions['tenant_id'] = $this->default_tenant_id;
				$transactions['id_services_in_income'] = $id_services_in_income;
				$transactions['created_by'] = $this->session->userdata('userid'); 
				$transactions['income_id'] = $income_id;
				$transactions['datetime'] = $input['datetime'];
				$transactions['account_id'] =isset($input['charges_debit_account'][$key]) && $input['charges_debit_account'][$key] > 0?$input['charges_debit_account'][$key]:null;
				$transactions['amount'] = ($input['charges'][$key] * $input['quantity'][$key]) * -1;
				$this->db->insert('s_transactions',$transactions);
			}
		}

		
		if($input['payment_status'] > 0){

			$transactions =array();
			$transactions['tenant_id'] = $this->default_tenant_id;
			$transactions['created_by'] = $this->session->userdata('userid'); 
			$transactions['income_id'] = $income_id;
			$transactions['datetime'] = $input['datetime'];
			$transactions['account_id'] =$input['account_id'];
			if($input['payment_status'] == 1){
				$transactions['amount'] =$input['bill_amount'];
			}else if($input['payment_status'] == 2){
				$transactions['amount'] =$input['paid_amount'];
			}
			$this->db->insert('s_transactions',$transactions);
			

		}
		// if($input['pending_amount'] > 0){
		// 	$transactions =array();
		// 	$transactions['created_by'] = $this->session->userdata('userid'); 
		// 	$transactions['income_id'] = $income_id;
		// 	$transactions['datetime'] = $input['datetime'];
		// 	$transactions['account_id'] =7;
		// 	$transactions['amount'] =$input['pending_amount'];
		// 	$this->db->insert('s_transactions',$transactions);
		// }
		// foreach($input['charges'] as $key=>$value){
			
		// 	for($i=0;$i<$input['quantity'][$key];$i++){
		// 		if($input['charges'][$key] > 0){
		// 			$transactions =array();
		// 			$transactions['tenant_id'] = $this->default_tenant_id;
		// 			$transactions['created_by'] = $this->session->userdata('userid'); 
		// 			$transactions['income_id'] = $income_id;
		// 			$transactions['datetime'] = $input['datetime'];
		// 			$transactions['account_id'] =isset($input['charges_debit_account'][$key]) && $input['charges_debit_account'][$key] > 0?$input['charges_debit_account'][$key]:null;
		// 			$transactions['amount'] = $input['charges'][$key] * -1;
		// 			$this->db->insert('s_transactions',$transactions);
		// 		}
		// 	}
		// }

		return print_r(json_encode($input));
	}
	public function add_payment_history(){

		$input = $this->input->post();	
			
		$sql1 = 'SELECT sum(amount) as paid_amount  FROM s_transactions WHERE income_id = "'.$input['income_id'].'" and status = 1 and amount > 0 and tenant_id='.$this->default_tenant_id.'';
		$query1 = $this->db->query($sql1);
		$paid_transactions=  $query1->row_array();

		$sql2 = 'SELECT *  FROM s_income WHERE id = "'.$input['income_id'].'" and status = 1 and tenant_id='.$this->default_tenant_id.'';
		$query2 = $this->db->query($sql2);
		$income=  $query2->row_array();
		
		if(!(strtotime($input['payment_date']) >= strtotime($income['datetime']))){
			return print_r(json_encode(array('status'=>false, 'msg'=> 'The payment date should be greater than or equal to sale date')));die;
		}

		$paid_amount = 0;
		if(isset($paid_transactions['paid_amount'])){
			$paid_amount = $paid_transactions['paid_amount'];
		}		
		if(!(($paid_amount + $input['paid_amount']) <= $income['bill_amount'])){
			$pending_amount = $income['bill_amount'] - $paid_amount;
			return print_r(json_encode(array('status'=>false, 'msg'=> 'The amount should be less than or equal to Rs.'.$pending_amount)));die;
		}

		$transactions =array();
		$transactions['created_by'] = $this->session->userdata('userid'); 
		$transactions['income_id'] = $input['income_id'];
		$transactions['account_id'] = $input['account_id'];
		$transactions['datetime'] = $input['payment_date'] ? $input['payment_date'] : date('Y-m-d H:i:s');
		$transactions['amount'] =$input['paid_amount'];
		$transactions['tenant_id'] =$this->default_tenant_id;
		$this->db->insert('s_transactions',$transactions);
		
		if( ceil($paid_amount + $input['paid_amount']) == ceil($income['bill_amount'])){
			$payment_status = 1;
		}else{
			$payment_status = 2;
		}
		$this->db->where('id',$input['income_id']);
		$update_data = array();
		$update_data['payment_status'] = $payment_status;
		$update_data['modified_by'] = $this->session->userdata('userid'); 
		$this->db->update('s_income', $update_data);

		return print_r(json_encode(array('status'=>true, 'msg'=> 'Added Successfully', 'data'=>$input)));die;
		
	}
	public function edit()
	{	
		$input = $this->input->post();

		$sql1 = 'SELECT *  FROM s_transactions WHERE income_id = "'.$input['income_id'].'" and status = 1 and amount > 0 and tenant_id='.$this->default_tenant_id.' order by datetime asc';
		$query1 = $this->db->query($sql1);
		$paid_transactions=  $query1->row_array();

		$sql11 = 'SELECT sum(amount) as total_paid_amount  FROM s_transactions WHERE income_id = "'.$input['income_id'].'" and status = 1 and amount > 0 and tenant_id='.$this->default_tenant_id.' order by datetime asc';
		$query11 = $this->db->query($sql11);
		$paid_transactions_sum =  $query11->row_array();
		if(isset($paid_transactions_sum['total_paid_amount']) && $input['bill_amount'] < $paid_transactions_sum['total_paid_amount']){
			return print_r(json_encode(array('status'=>false, 'msg'=> 'The bill amount should be less than or equal to paid amount')));die;
		}


		if(isset($paid_transactions['datetime'])){
			if(!(strtotime($input['datetime']) <= strtotime($paid_transactions['datetime']))){
				return print_r(json_encode(array('status'=>false, 'msg'=> 'The sale date should be less than or equal to payment date')));die;
			}
		}

		$sql2 = 'SELECT *  FROM s_income WHERE id = "'.$input['income_id'].'" and status = 1 and tenant_id='.$this->default_tenant_id.'';
		$query2 = $this->db->query($sql2);
		$income=  $query2->row_array();

		if(!($income['payment_status'] == "1") && $input['payment_responsible_by']=="0"){
			return print_r(json_encode(array('status'=>false, 'msg'=> 'Please select payment responsible person')));die;
		}


		$payment_status = 0;
		$total_paid_amount = isset($paid_transactions_sum['total_paid_amount']) ? $paid_transactions_sum['total_paid_amount'] : 0;
		if($total_paid_amount == $input['bill_amount']){
			$payment_status = 1;
		}else if($total_paid_amount < $input['bill_amount']){
			$payment_status = 2;
		}
		
		$this->db->where('id',$input['income_id']);
		$update_data = array();
		$update_data['gst_bill'] = $input['gst_bill'];
		$update_data['payment_responsible_by'] = $input['payment_responsible_by'];
		$update_data['comments'] = $input['comments'];
		$update_data['customer_id'] = $input['customer_id'];
		$update_data['datetime'] = $input['datetime'];
		$update_data['modified_by'] = $this->session->userdata('userid'); 
		$update_data['bill_amount'] = $input['bill_amount'];
		$update_data['payment_status'] = $payment_status;
		$this->db->update('s_income', $update_data);

		$active_id_services_in_income = array();
		// $bill_amount = 
		foreach($input['service_name'] as $key=>$value){
			
			$services_in_income = array();
			$services_in_income['tenant_id'] = $this->default_tenant_id;
			$services_in_income['income_id'] = $input['income_id'];
			$services_in_income['service_id'] = $input['service_name'][$key];
			$services_in_income['ref_no'] = $input['ref_no'][$key];
			$services_in_income['work_status'] = $input['status'][$key];
			$services_in_income['quantity'] = $input['quantity'][$key];
			$services_in_income['charges'] = $input['charges'][$key];
			if(isset($input['charges_debit_account'][$key])){

				$services_in_income['charges_debit_account_id'] = $input['charges_debit_account'][$key];
			}
			$services_in_income['commission'] = $input['commission'][$key];
			// $services_in_income['total_amount'] = ($input['charges'][$key] + $input['commission'][$key]) *  $input['quantity'][$key];
			$services_in_income['gst_rate'] = $input['gst_rate'][$key];
			$services_in_income['total_amount'] = $input['charges'][$key] + $input['commission'][$key];
			if($input['gst_bill'] > 0){
				$services_in_income['gst_value'] = $services_in_income['gst_rate'] / 100 * $services_in_income['commission'];
				$services_in_income['total_amount'] += $services_in_income['gst_value'];
			}
			$services_in_income['total_amount'] = $services_in_income['total_amount'] *  $input['quantity'][$key];

			if(!empty($input['id_services_in_income'][$key])){
				$active_id_services_in_income[] = $input['id_services_in_income'][$key];
				$services_in_income['modified_by'] = $this->session->userdata('userid'); 
				$this->db->where('id',$input['id_services_in_income'][$key])->where('tenant_id', $this->default_tenant_id);
				$this->db->update('s_services_in_income', $services_in_income);


				$transactions =array();
				$transactions['modified_by'] = $this->session->userdata('userid'); 
				$transactions['datetime'] = $input['datetime'];
				$transactions['account_id'] =isset($input['charges_debit_account'][$key]) && $input['charges_debit_account'][$key] > 0?$input['charges_debit_account'][$key]:null;
				$transactions['amount'] = ($input['charges'][$key] * $input['quantity'][$key]) * -1;
				$this->db->where('id_services_in_income',$input['id_services_in_income'][$key])->where('income_id', $input['income_id'])->where('tenant_id', $this->default_tenant_id);
				$this->db->update('s_transactions',$transactions);

			}else{
				$services_in_income['created_by'] = $this->session->userdata('userid'); 
				$this->db->insert('s_services_in_income',$services_in_income);
				$id_services_in_income = $this->db->insert_id();
				$active_id_services_in_income[] = $id_services_in_income;

				if($input['charges'][$key] > 0){
					$transactions =array();
					$transactions['tenant_id'] = $this->default_tenant_id;
					$transactions['id_services_in_income'] = $id_services_in_income;
					$transactions['created_by'] = $this->session->userdata('userid'); 
					$transactions['income_id'] = $input['income_id'];
					$transactions['datetime'] = $input['datetime'];
					$transactions['account_id'] =isset($input['charges_debit_account'][$key]) && $input['charges_debit_account'][$key] > 0?$input['charges_debit_account'][$key]:null;
					$transactions['amount'] = ($input['charges'][$key] * $input['quantity'][$key]) * -1;
					$this->db->insert('s_transactions',$transactions);
				}

			}
		}
		$this->db->where_not_in('id', $active_id_services_in_income);
		$this->db->where('income_id', $input['income_id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_services_in_income', array(
			'status'=>-1,
			'modified_by'=>$this->session->userdata('userid')
		));

		$this->db->where_not_in('id_services_in_income', $active_id_services_in_income);
		$this->db->where('income_id', $input['income_id'])->where('tenant_id', $this->default_tenant_id)->where('amount <', 0);
		$this->db->update('s_transactions', array(
			'status'=>-1,
			'modified_by'=>$this->session->userdata('userid')
		));

		return print_r(json_encode(array('status'=>true, 'msg'=> 'Updated successfully', 'data'=>$input)));die;
	}

	public function delete_payment_history()
	{
		$input = $this->input->post();
		$data = array();
		
		
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_transactions', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		
		$payment_status = 0;
		$transactions = $this->db->where('income_id',$input['income_id'])->where('amount', '> 0')->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->get('s_transactions')->result_array();
		if(count($transactions)>0){
			$payment_status = 2;
		}

		$this->db->where('id',$input['income_id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_income', array('payment_status'=>$payment_status, 'modified_by'=>$this->session->userdata('userid')));

		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function print($income_id){
		$invoice_data = $this->generalhelper->getInvoiceData($income_id);
		$invoice_data['invoice_prefix'] = $this->generalhelper->getSettings('INVOICE_PREFIX');
		// $html = $this->load->view('pdf/invoice', $invoice_data, TRUE);
		// echo $html;die;
		// print_r($invoice_data);die;
		// error_reporting(E_ALL);
		// ini_set('display_errors', '1');
		$this->load->library('TcpdfHelper');
		// echo $income_id;die;

		$PDF_HEADER_TITLE = $invoice_data['tenant']->tenant_name;
		$PDF_HEADER_STRING =  $invoice_data['tenant']->addr_line1 . " \nMOB:- ".$invoice_data['tenant']->mobile. ($invoice_data['tenant']->mobile2?', '.$invoice_data['tenant']->mobile2:'') .", GSTIN/UIN: ". $invoice_data['tenant']->gstin ." \nState Name : ".$invoice_data['tenant']->state.", Code : ".$invoice_data['tenant']->state_code.",  E-Mail : ". $invoice_data['tenant']->email;

		// Create a new PDF document
		$pdf = new TcpdfHelper(PDF_PAGE_ORIENTATION, PDF_UNIT, array(50.8, PDF_PAGE_FORMAT), true, 'UTF-8', false);

		// Set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Srina');
		$pdf->SetTitle('Invoice');
		$pdf->SetSubject('Invoice');
		
		$pdf->setHeaderFont(array('', '', 9));
		$pdf->SetHeaderData('../../../../../'. $invoice_data['tenant']->logo, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
		// $pdf->SetPrintHeader(false);

		// Set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// Set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// Set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// Set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// Set font
		$pdf->SetFont('helvetica', '', 9);

		// Add a page
		$pdf->AddPage();

		// Set title
		$pdf->SetFont('helvetica', 'B', 12);
		$pdf->Cell(0, 10, $invoice_data['invoice']->gst_bill ? 'TAX INVOICE': 'INVOICE', 0, 1, 'C');

		// Invoice content
		$pdf->SetFont('helvetica', '', 9);
		
		$html = $this->load->view('pdf/invoice', $invoice_data, TRUE);

		$pdf->writeHTML($html, true, false, true, false, '');

		// Close and output PDF document
		$pdf->Output('invoice.pdf', 'I');

	}

}