<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $default_tenant_id;
	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$this->default_tenant_id = $this->session->userdata('default_tenant_id'); 
		if(empty($this->default_tenant_id)){
			echo "You do not have access to view this page."; die;
		}
		$this->load->library('GeneralHelper');
	}
	public function index()
	{
		$data = array();
		$data['customers'] = $this->db->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->get('s_customers')->result_array();
		// $sql = 'SELECT *, b.name as referred_by_name  FROM s_customers as a
		// 		LEFT JOIN s_customers as b ON a.id=b.referred_by
		// 		WHERE a.status in (1,0) 
		// 		';
		// $query = $this->db->query($sql);
		// $data['customers']=  $query->result_array();
		$data['ENABLE_GST'] = $this->generalhelper->getSettings('ENABLE_GST');

 
		$this->load->view('customers',$data);
	}
	public function list1()
	{
		$data = array();
		// $data['data'] = $this->db->where_in('status', [1,0])->where('tenant_id', $this->default_tenant_id)->get('s_customers')->result_array();
		$sql = 'SELECT s_customers.*, max(s_income.datetime) as recently_at  FROM s_customers
				LEFT JOIN s_income ON s_customers.id=s_income.customer_id
				WHERE s_customers.status in (1,0) and s_customers.tenant_id = '.$this->default_tenant_id.' group by s_customers.id
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['data']=  $query->result_array();

		return print_r(json_encode($data));
	}
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_customers', array('status'=>-1,  'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$data['data'] =	$this->db->get('s_customers')->row_array();
		$data['referred_by'] =	array();
		if($data['data']['referred_by']){
			$this->db->where('id',$data['data']['referred_by']);
			$data['referred_by'] =	$this->db->get('s_customers')->row_array();

		}
		return print_r(json_encode($data));
	}
	public function add()
	{
		$input = $this->input->post();
		$response = array();

		$this->db->where('mobile',$input['mobile'])->where('tenant_id', $this->default_tenant_id);
		$this->db->where_in('status', [1,0]);
		$mobile_number_row =	$this->db->get('s_customers')->num_rows();
		if($mobile_number_row>0){
			$response['status'] = 'failure';
			$response['msg'] = 'Mobile number already exists';
			print_r(json_encode($response));die;
		}

		// print_r($input);die;
		$input['created_by'] = $this->session->userdata('userid'); 
		$input['tenant_id'] = $this->default_tenant_id; 
		$this->db->insert('s_customers',$input);
		$response['status'] = 'success';
		$response['msg'] = 'Added Successfully';
		return print_r(json_encode($response));
	}
	public function edit()
	{	
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 
		$this->db->where('id',$input['id'])->where('tenant_id', $this->default_tenant_id);
		$this->db->update('s_customers',$input);
		return print_r(json_encode($input));
	}

}