CREATE TABLE `s_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL
);

ALTER TABLE `s_services` ADD `category_id` INT NOT NULL AFTER `commission`, ADD `required_documents` TEXT NOT NULL AFTER `category_id`, ADD `url` VARCHAR(100) NOT NULL AFTER `required_documents`, ADD `steps` TEXT NOT NULL AFTER `url`;



ALTER TABLE `s_users` ADD `is_admin` INT NOT NULL DEFAULT '0' AFTER `password`;

ALTER TABLE `s_services` ADD `fees_account_id` INT NULL DEFAULT NULL AFTER `fees`;

ALTER TABLE `s_income` ADD `paper_count` INT NOT NULL DEFAULT '0' AFTER `payment_responsible_by`;

ALTER TABLE `s_services_in_income` ADD `quantity` INT NOT NULL DEFAULT '0' AFTER `work_status`;

#ALTER TABLE `s_assets` ADD `id_service` INT NULL DEFAULT NULL AFTER `stock`;
#ALTER TABLE `s_services` ADD `asset_id` INT NULL DEFAULT NULL AFTER `category_id`;



