
function addemployees(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    $.ajax({url: "employees/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        getemployeesList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
  }
function viewemployees(id){
    
    $.ajax({url: "employees/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .mobile').html(result['data']['mobile']);
        $('#view1 .status').html( (result['data']['status'] == 1 ? 'Active' : 'In Active' ));
    }});
  }
function deleteemployees(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "employees/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            getemployeesList();
          }});
      }
    }

function getemployeesList(){
    $.ajax({url: "employees/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td>'+element['name']+'</td>';
            html += '<td>'+element['mobile']+'</td>';
            html += '<td>'+ (element['status'] == 1 ? 'Active' : 'In Active' ) +'</td>';
            html += '<td><button type="button" class="btn btn-primary" onclick="viewemployees('+element['id']+'); return false">view</button> ';
           // html += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Edit</button> ';
            html += '<button type="button" class="btn btn-primary"onclick="deleteemployees('+element['id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#employees-tbl').DataTable().destroy();
        $('#employees-tbl tbody').html(html);
        $('#employees-tbl').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
          });
      }});
    }
      
