
function addCategory(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("p");
    $.ajax({url: "categories/add?p="+c, method:'POST', dataType: 'json',data:formData,success: function(result){
        getCategoriesList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function viewCategory(id){
    
    $.ajax({url: "categories/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .fees').html(result['data']['fees']);
        $('#view1 .commission').html(result['data']['commission']);
        $('#view1 .total').html(parseInt(result['data']['fees']) + parseInt(result['data']['commission']));
    }});
}
function editCategory(id){
    
    $.ajax({url: "categories/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);
        $('#edit input[name=fees]').val(result['data']['fees']);
        $('#edit input[name=commission]').val(result['data']['commission']);
    }});
}
function deleteCategory(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "categories/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            getCategoriesList();
           }});
      }
}
function getCategoriesList(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("p");


    $.ajax({url: "categories/list2?p="+c, dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td><a href="Categories?p='+element['id']+'">'+element['name']+'</a></td>';
            html += '<td>';
            // <button type="button" class="btn btn-primary" onclick="viewCategory('+element['id']+'); return false">view</button> ';
        //    html += '<button type="button" class="btn btn-primary" onclick="editCategory('+element['id']+'); return false">Edit</button> ';
        if(element['id'] > 3){
            html += '<button type="button" class="btn btn-primary" onclick="deleteCategory('+element['id']+', \''+element['name']+'\'); return false" '+(isAdmin!=1?'disabled':'')+'>Delete</button>'
        }
        html += '<td>';
        html += '</tr>'
        });
        $('#categories-tbl').DataTable().destroy();
        $('#categories-tbl tbody').html(html);
        $('#categories-tbl').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "bDestroy": true
          });
      }});
}

