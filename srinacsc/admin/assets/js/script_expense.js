


function addexpense(){
    $('#add form :submit').prop('disabled', true);
  var formData = $('#add form').serialize();

  $.ajax({url: "expense/add", method:'POST', dataType: 'json',data:formData,success: function(result){
      getexpenseList();
      $('#add form')[0].reset();
      $('#add .msg').html('Added Successfully');
      // $('#add').modal('hide');
      $('#add form :submit').prop('disabled', false);
  }});
}
function viewexpense(id){
    
    $.ajax({url: "expense/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['expense_id']);
        $('#view1 .datetime').html(result['data']['datetime']);
        
        $('#view1 .expense-tr').hide();
        $('#view1 .product-tr').hide();
        $('#view1 .stock-tr').hide();
        var expense_type = '';
        if(result['data']['expense_type']=='0'){
          expense_type = 'Shop Expense';
          $('#view1 .expense').html(result['data']['master_expense_name']);
          $('#view1 .expense-tr').show();
        }else if(result['data']['expense_type']=='1'){
          $('#view1 .product-tr').show();
          $('#view1 .stock-tr').show();
          expense_type = 'Purchase Entry';
        }else{
          expense_type = '';
        }
        $('#view1 .expense_type').html(expense_type);

        $('#view1 .asset_name').html(result['data']['asset_name']);
        $('#view1 .count').html(result['data']['count']);
        $('#view1 .amount').html(result['data']['amount']);
        $('#view1 .additional_info').html(result['data']['additional_info']);
    }});
  }
function deleteexpense(id, name){
    if (confirm('Are you sure to delete id '+id+'?') == true) { 
         $.ajax({url: "expense/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getexpenseList();
           }});
      }
    }

function getexpenseList(){
    $.ajax({url: "expense/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['expense_id']+'</td>';
            html += '<td>'+element['datetime']+'</td>';
            if(element['expense_type']=='0'){
              html += '<td>Shop Expense</td>';
            }else if(element['expense_type']=='1'){
              html += '<td>Purchase Entry</td>';
            }else{
              html += '<td></td>';
            }
            
            // html += '<td>'+element['count']+'</td>';
            html += '<td>'+element['amount']+'</td>';
            // html += '<td>'+element['additional_info']+'</td>';
            html += '<td>';
             html += '<button type="button" class="btn btn-primary" onclick="viewexpense('+element['expense_id']+'); return false">view</button>';
           // html += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Edit</button> ';
            html += ' <button type="button" class="btn btn-primary" onclick="deleteexpense('+element['expense_id']+', \''+element['name']+'\'); return false" >Delete</button></td>'
            html += '</tr>'
        });
        $('#expense-tbl').DataTable().destroy();
        $('#expense-tbl tbody').html(html);
        $('#expense-tbl').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
          });
      }});
}

expenseTypeChanges();
function expenseTypeChanges(){
  $('.product-sec').hide();
  $('.asset-sec').hide();
  $('.expense-details-sec').hide();
  $('.stock-sec').hide();
  // $('select[name=product_id]').val('');
  var val = $('select[name=expense_type] option:selected').val(); 
  if(val == "0"){
    $('.expense-details-sec').show();
      // $('#add1 select[name=account_id]').val($('#add1 select[name=account_id] option.pending').val());
      // $('.payment-responsible-by').show();
  }else if(val == "1"){
    $('.product-sec').show();
    // $('.asset-sec').hide();
    $('.stock-sec').show();
  }

}