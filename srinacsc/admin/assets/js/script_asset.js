
      function addassets(){
        $('#add form :submit').prop('disabled', true);
        var formData = $('#add form').serialize();
        $.ajax({url: "assets/add", method:'POST', dataType: 'json',data:formData,success: function(result){
            getassetsList();
            $('#add form')[0].reset();
            $('#add .msg').html('Added Successfully');
            $('#add form :submit').prop('disabled', false);
            // $('#add').modal('hide');
        }});
    }

function viewassets(id){
    
    $.ajax({url: "assets/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .stock').html(result['data']['stock']);
    }});
  }
  
function deleteassets(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "assets/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getassetsList();
          }});
      }
    }
function editAsset(id){
    removeMessage();
    $.ajax({url: "assets/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);
        $('#edit input[name=stock]').val(result['data']['stock']);
    }});
}
function confirmEditAssets(){
    $('#edit form :submit').prop('disabled', true);
    var formData = $('#edit form').serialize();
    $.ajax({url: "assets/edit", method:'POST', dataType: 'json',data:formData,success: function(result){
        getassetsList();
        $('#edit .msg').html('Updated Successfully');
        $('#edit form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function getassetsList(){
    $.ajax({url: "assets/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td>'+element['name']+'</td>';
            html += '<td>'+element['stock']+'</td>';
            html += '<td><button type="button" class="btn btn-primary" onclick="viewassets('+element['id']+'); return false">view</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="editAsset('+element['id']+'); return false">Edit</button> ';
            html += ' <button type="button" class="btn btn-primary" onclick="deleteassets('+element['id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#assets-tbl').DataTable().destroy();
        $('#assets-tbl tbody').html(html);
        $('#assets-tbl').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
          });
      }});
}