function viewIncomeDetails(transactionDate){
    
    $.ajax({url: "dashboard/getIncomeDetails", method:'POST',data:{'transactionDate': transactionDate},success: function(result){
        $('.modal-body .msg').text('');
        $('#income').modal('show');
        $('#income .modal-body').html(result);
    }});
}
function viewExpenseDetails(transactionDate){
    
    $.ajax({url: "dashboard/getExpenseDetails", method:'POST',data:{'transactionDate': transactionDate},success: function(result){
        $('.modal-body .msg').text('');
        $('#expence').modal('show');
        $('#expence .modal-body').html(result);
    }});
}

function getTransactionDetails(transactionDate, accountId){
    if(accountId=="0"){
        var endpoint = "dashboard/getPendingAmountTransactions";
    }else{
        var endpoint = "dashboard/getTransactionDetails";
    }
    $.ajax({url: endpoint, method:'POST',data:{'transactionDate': transactionDate, 'accountId': accountId},success: function(result){
        $('.modal-body .msg').text('');
        $('#transactions').modal('show');
        $('#transactions .modal-body').html(result);
    }});
}
function updateGoogleSheet(){
    $('.update-google-sheet-loader').show();
    $('.update-google-sheet-btn').prop('disabled', true);
    $.ajax({url: 'dashboard/updateGoogleSheet', method:'POST',data:{},success: function(result){
    $('.update-google-sheet-loader').hide();
    $('.update-google-sheet-btn').prop('disabled', false);
    }});
}