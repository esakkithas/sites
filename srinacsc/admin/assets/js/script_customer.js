
function addcustomers(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    $.ajax({url: "customers/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        if(result.status=='success'){
            getcustomersList();
            $('#add form')[0].reset();
            $('#add .msg').html(result.msg);
        }else{
            alert(result.msg);
        }
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function viewcustomers(id){
    
    $.ajax({url: "customers/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .customer-id').html(result['data']['customer_id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .mobile').html(result['data']['mobile']);
        $('#view1 .address').html(result['data']['address']);
        $('#view1 .referred_by').html('');
        if(result['referred_by']['name'] != undefined){
            $('#view1 .referred_by').html(result['referred_by']['name'] + ' ( ' + result['referred_by']['id'] + ' ) ');
        }
       
    }});

}
function deletecustomers(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "customers/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getcustomersList();
          }});
      }
    }
function getcustomersList(){
    $.ajax({url: "customers/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td>'+element['customer_id']+'</td>';
            html += '<td>'+element['name']+'</td>';
            html += '<td>'+element['mobile']+'</td>';
            html += '<td>'+element['address']+'</td>';
            html += '<td>'+element['recently_at']+'</td>';
            html += '<td><button type="button" class="btn btn-primary"onclick="viewcustomers('+element['id']+'); return false">view</button> ';
            html += ' <button type="button" class="btn btn-primary" onclick="editCustomer('+element['id']+'); return false">Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deletecustomers('+element['id']+', \''+element['name']+'\'); return false">Delete</button></td>';
            html += '</tr>'
        });
        $('#customers-tbl').DataTable().destroy();
        $('#customers-tbl tbody').html(html);
        $('#customers-tbl').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
          });
    }});
  }
  function editCustomer(id){
    
    $.ajax({url: "customers/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);
        $('#edit input[name=mobile]').val(result['data']['mobile']);
        $('#edit textarea[name=address]').val(result['data']['address']);
        $('#edit input[name=customer_id]').val(result['data']['customer_id']);
        $('#edit input[name=borrow_limit]').val(result['data']['borrow_limit']);
        $('#edit select[name=notification]').val(result['data']['notification']);

        $('#edit input[name=gstin]').val(result['data']['gstin']);
        $('#edit input[name=state]').val(result['data']['state']);
        $('#edit input[name=state_code]').val(result['data']['state_code']);
        $('#edit input[name=email]').val(result['data']['email']);

        $('#edit select[name=referred_by]').val(result['data']['referred_by']);

 
    }});
}
function confirmEditCustomer(){
    $('#edit form :submit').prop('disabled', true);
    var formData = $('#edit form').serialize();
    $.ajax({url: "customers/edit", method:'POST', dataType: 'json',data:formData,success: function(result){
        getcustomersList();
        $('#edit .msg').html('Updated Successfully');
        $('#edit form :submit').prop('disabled', false);
    }});
}
