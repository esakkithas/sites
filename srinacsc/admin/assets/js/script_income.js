
  function addincome(){
    var paid_amount = $('#add1 input[name=paid_amount]').val();
    var account_id = $('#add1 select[name=account_id]').val();
    var payment_status = $('.payment_status option:selected').val();
    var account_name =  $('#add1 select[name=account_id] option[value="' + account_id + '"]').text().trim();
    var gst_bill = $('#add1 input[name=gst_bill]:checked').length;

    if(payment_status == "1" || ( payment_status == "2")){
         if(account_name == "Pending" || account_name == "Select"){
             alert('Please select payment method');
             return false;
         }
        
    }
    if(payment_status == "2"){
        if(parseFloat(paid_amount) <= 0){
            alert('Please enter amount');
            return false;
        }
    }
    var pending_amount = $('.pending-amount').val();
    if(parseFloat(pending_amount) >0){
        var payment_responsible_by = $("#payment_responsible_by-hidden").val();
            if(payment_responsible_by == undefined || payment_responsible_by == ""){
                alert('Please select "Payment Responsible By"');
                return false;
            }
    }
    var validation_status = true;
    $('#add1 #serviceslisttbl tbody tr').each(function(index, value) {
        var charges = $(this).find('.charges').val();
        if(parseInt(charges) > 0){ 
            var fees_account_id = $(this).find('.fees_account_id option:selected').val();
            // console.log('fees_account_id');
            // console.log(fees_account_id);
            if(fees_account_id == undefined || fees_account_id == ""){
                validation_status = false;
                alert('Please select debit account');
                return false;
            }
        }
    });

    

    setTimeout(function(){

        if(validation_status == false){
            return false;
        }
        $('#add1 form :submit').prop('disabled', true);
        var formData = $('#add1 form').serialize();
        $.ajax({url: "income/add", method:'POST', dataType: 'json',data:formData+'&gst_bill='+gst_bill,success: function(result){
            getincomeList();
            $('#add1 form')[0].reset();
            $('.row-gst-amount').text('0');
            $('.row-gst-amount').attr('title', '');
            $('.total-gst-rate').text('Rs. 0');
            alert('Added Successfully');
            $('#add1 form :submit').prop('disabled', false);
            // $('#add').modal('hide');
            

            $('#serviceslisttbl .row-total-amount').html('Rs. 0');
            $('.total-bill-amount').html('Rs. 0');

        }});

    }, 1);
}
function editIncome(id){
    
    $.ajax({url: "income/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#editForm').modal('show');
        $('#editForm input[name=id]').val(result['data']['income_id']);

        var gst_bill_bool = false;
        if(result['data']['gst_bill'] > 0){
            gst_bill_bool = true;
        }
        $('#editForm input[name=gst_bill]').prop('checked', gst_bill_bool);

        // $('#editForm .row-gst-rate').val(result['data']['gst_bill']);
        

        var print_baseurl = $('#editForm .print-button').attr('data-baseurl');
        $('#editForm .print-button').attr('href', print_baseurl + result['data']['income_id']);
        
        $('#editForm input[name=datetime]').val(result['data']['datetime'].replace(/\s+/g, 'T'));
        // $('#editForm input[name=customer_id]').val(result['data']['customer_id']);
        datalist('customer-id-edit', result['data']['customer_id']);
        $('.tfoot-total').text(result['data']['bill_amount']);
        $('#editForm select[name=payment_status]').val(result['data']['payment_status']);
        paymentStatusChangesEdit();
        // console.log('..');
        // console.log(result['data']);
        // console.log('..');
        $('#editForm #payment_responsible_by-edit').val('');
        if(result['data']['payment_status'] == "0"){
            $('#editForm select[name=payment_status]').removeAttr('disabled');
            $('#editForm select[name=account_id]').removeAttr('disabled');
           
            // $('#editForm select[name=account_id]').val(result['pending']['account_id']);
            // $('#editForm input[name=pending_amount]').val(result['pending']['amount']);


            $('#editForm #payment_responsible_by-edit').removeAttr('disabled');
            $('#editForm input[name=comments]').removeAttr('disabled');

        }else if(result['data']['payment_status'] == "1"){
            // $('#editForm select[name=payment_status]').attr('disabled', true);
            // $('#editForm select[name=account_id]').attr('disabled', true);
            // $('#editForm select[name=account_id]').val(result['paid']['account_id']);

            
            // $('#editForm #payment_responsible_by-edit').attr('disabled', true);
            // $('#editForm input[name=comments]').attr('disabled', true);

        } else if(result['data']['payment_status'] == "2"){
            $('#editForm #payment_responsible_by-edit').removeAttr('disabled');
            $('#editForm input[name=comments]').removeAttr('disabled');
            
            $('#editForm select[name=payment_status]').removeAttr('disabled');
            $('#editForm select[name=account_id]').removeAttr('disabled');
            // $('#editForm input[name=payment_responsible_by]').val(result['data']['payment_responsible_by']);
            // if(parseInt(result['data']['payment_responsible_by']) > 0){

            //     datalist('payment_responsible_by-edit', result['data']['payment_responsible_by']);
            // }
        }
        $('#editForm input[name=payment_responsible_by]').val(result['data']['payment_responsible_by']);
        if(parseInt(result['data']['payment_responsible_by']) > 0){

            datalist('payment_responsible_by-edit', result['data']['payment_responsible_by']);
        }
        
        var sale_pending_amount = 0;
        if(result['data']['payment_status'] == "0"){
            sale_pending_amount = result['data']['bill_amount'];
        }else if(result['data']['payment_status'] == "2"){
            sale_pending_amount = parseFloat(result['data']['bill_amount']) - parseFloat(result['paid_amount']);
        }
        $('#editForm .sale-pending-amount').html('( Pending : ' + sale_pending_amount + ')');
        $('#editForm input[name=paid_amount]').attr('max', sale_pending_amount);

        if(sale_pending_amount == 0){
            $('.payment-history-form-div').hide();
        }else{
            $('.payment-history-form-div').show();
        }
        
        // if(result['data']['payment_status'] == "1"){
        //     $('.payment-date').hide();
        // }else{
        //     $('.payment-date').show();
        // }


        $('#editForm input[name=paper_count]').val(result['data']['paper_count']);
        $('#editForm input[name=comments]').val(result['data']['comments']);
        // $('#editForm .customer').html(result['data']['customer_name']);
        // $('#editForm .bill_amount').html(result['data']['bill_amount']);
        // $('#editForm .pending_amount').html(result['data']['pending_amount']);
        // $('#editForm .payment_status').html(result['data']['payment_status'] == 0 ? 'Pending' : 'Paid');
        // console.log(result['serviceslist']);
        // var tblhtml = '';
           var selector = $('#editForm #serviceslisttbl tbody tr:first');
           selector.remove();
        $(".modal.show #serviceslisttbl tbody").html('');
        $("#editForm .payment-history-table tbody").html('');
        for( sl in result['serviceslist']) {
            var value = result['serviceslist'][sl];
            var html = selector.clone();
            html.find('.service-name').val(value['service_name']);
            html.find('.service-name-hidden').val(value['service_id']);
            html.find('.id-services-in-income').val(value['id_services_in_income']);
            html.find('.quantity').val(value['quantity']);
            html.find('.charges').val(value['charges']);
            html.find('.commission').val(value['commission_new']);
            // console.log(value);
            html.find('.row-gst-rate').val(value['gst_rate']);
            html.find('.row-total-amount').html(value['total_amount']);
            html.find('.fees_account_id').val(value['charges_debit_account_id']);

            // html.find('.fees_account_id').val(value['fees_account_id']);
            html.find('.custom-select1').val(value['work_status']);
            html.find('.refno').val(value['ref_no']);
            html.appendTo(".modal.show #serviceslisttbl tbody");

            // tblhtml += '<tr><td>'+value['service_name']+'</td><td>'+value['amount']+'</td><td>'+value['status']+'</td><td>'+value['ref_no']+'</td></tr>';
          }
          
            if($('.modal.show .serviceslisttbl tbody tr').length==1){
                $('.modal.show .serviceslisttbl .fa-trash').hide();
            }else{
                $('.modal.show .serviceslisttbl .fa-trash').show();
            }

          calculation('#editForm');
        //   console.log('fdsa', result['transactions'].length);
        //   if(result['transactions'].length > 1){
        //     $('.payment-history').show();
        //   }else{
        //     $('.payment-history').hide();
        //   }
        //   var selector = $('#editForm .payment-history-table tbody tr:first');
          $('#editForm .payment-history-table tbody').html('');
          for( trans in result['transactions']) {
            var value = result['transactions'][trans];
       
            var html = '<tr class="transaction'+value['transaction_id']+'"><td>'+value['datetime']+'</td>'
            html += '<td>'+value['amount']+'</td>'
            html += '<td>'+value['account_name']+'</td>'
            html += '<td><a href="#"><i class=" nav-icon fas fa-trash" onclick="deletePaymentHistory('+value['transaction_id']+', \''+value['amount']+'\', \''+value['income_id']+'\'); return false"></i></a></td></tr>'
            // var html = selector.clone();
            
            // html.find('.payment-d').html(value['datetime']);
            // html.find('.amount').html(value['amount']);
            // html.find('.account').html(value['account_name']);
            $("#editForm .payment-history-table tbody").append(html);
          }

        // //   console.log(tblhtml);
        //   $("#serviceslist tbody").html(tblhtml);


        // var transhtml = ''
        // console.log('transactions');
        // console.log(result['transactions']);
        //   for( t in result['transactions']) {
        //     var value = result['transactions'][t];
        //     transhtml += '<tr><td>'+value['datetime']+'</td><td>'+value['account_id']+'</td><td>'+value['amount']+'</tr>';
        //   }
        //   $(".payment-transactions tbody").html(transhtml);

       
    }});
  }
  function addPaymentHistory(){
    var income_id =   $('#editForm input[name=id]').val();
    var payment_date = $('.payment-history-form-div input[name=payment_date]').val();
    if(payment_date == ""){
        alert('Please select payment date');
        return false;
    }
    var paid_amount = $('.payment-history-form-div input[name=paid_amount]').val();
    if( !(parseFloat(paid_amount) > 0) ){
        alert('Please enter valid amount');
        return false;
    }
    var account_id = $('.payment-history-form-div select[name=account_id]').val();
    if(account_id == ""){
        alert('Please select Payment Method');
        return false;
    }
    $.ajax({
        url: "income/add_payment_history", 
        method:'POST', 
        dataType: 'json', 
        data:{
            income_id:income_id, 
            payment_date:payment_date, 
            paid_amount:paid_amount, 
            account_id: account_id
        },success: function(result){
            alert(result.msg);
            if(result.status==true){
                getincomeList();
                editIncome(income_id);
                $('.payment-history-form-div input[name=paid_amount]').val(0);
            }
    }});

  }
  function confirmEditIncome(){ 
    var income_id =   $('#editForm input[name=id]').val();
    // var payment_responsible_by = $('#editForm input[name=payment_responsible_by]').val();
    // var comments = $('#editForm input[name=comments]').val();
    var formData = $('#edit-sale').serialize();
    var gst_bill = $('#editForm input[name=gst_bill]:checked').length;
    $.ajax({
        url: "income/edit", 
        method:'POST', 
        dataType: 'json', 
        data:formData+'&income_id='+income_id+'&gst_bill='+gst_bill,
        success: function(result){
            if(result.status==true){
                getincomeList();
                editIncome(income_id);
            }
            alert(result.msg);
    }});
  }
  function viewincome(id){
    console.log('id');
    $.ajax({url: "income/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .datetime').html(result['data']['datetime']);
        $('#view1 .customer').html(result['data']['customer_name']);
        $('#view1 .bill_amount').html(result['data']['bill_amount']);
        $('#view1 .pending_amount').html(result['data']['pending_amount']);
        $('#view1 .payment_status').html(result['data']['payment_status'] == 0 ? 'Pending' : 'Paid');
        $('#view1 .account').html(result['data']['payment_responsible_by']);
        $('#view1 .payment_responsible_by').html(result['data']['payment_responsible_by']);

        var tblhtml = '';
        for( sl in result['serviceslist']) {
            var value = result['serviceslist'][sl];
            tblhtml += '<tr><td>'+value['service_name']+'</td><td>'+value['amount']+'</td><td>'+value['status']+'</td><td>'+value['ref_no']+'</td></tr>';
          }
          $("#serviceslist tbody").html(tblhtml);
        
       


        //   console.log(tblhtml);
       
    }});
  }
  function deleteincome(id, name){
    if (confirm('Are you sure to delete id '+id+'?') == true) { 
         $.ajax({url: "income/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getincomeList();
          }});
      }
    }
function deletePaymentHistory(id, amount, income_id){
    if (confirm('Are you sure to delete Rs.'+amount+'?') == true) {
        $.ajax({url: "income/delete_payment_history", method:'POST', dataType: 'json',data:{'id': id, 'income_id': income_id},success: function(result){
            $('tr.transaction'+id).remove();
            editIncome(income_id);
         }});
     }
}
function getincomeList(){
    var from_date = findGetParameter('from_date');
    var to_date = findGetParameter('to_date');
    $.ajax({url: "income/list1", data: {'from_date': from_date, 'to_date': to_date}, dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr class="status'+element['status']+'">';
            html += '<td>'+element['income_id']+'</td>';
            html += '<td>'+element['datetime']+'</td>';
            html += '<td>'+element['customer_name']+'</td>';
            html += '<td>'+element['bill_amount']+'</td>';      
            var payment_status = '';
            if(element['payment_status']==0){
                payment_status = 'Pending'; 
            }else if(element['payment_status']==1){
                payment_status = 'Paid'; 
            } else if(element['payment_status']==2){
                payment_status = 'Partial Payment'; 
            }   
            html += '<td>'+ payment_status +'</td>';            
            html += '<td><button style="display:none" type="button" class="btn btn-primary" onclick="viewincome('+element['income_id']+'); return false">view</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="editIncome('+element['income_id']+'); return false">Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deleteincome('+element['income_id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#income-tbl').DataTable().destroy();
        $('#income-tbl tbody').html(html);
        $('#income-tbl').DataTable({
            // "paging": true,
            // "lengthChange": false,
            // "searching": true,
            // "ordering": true,
            // "info": true,
            // "autoWidth": false,
            // "responsive": true, 
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, 'All'],
            ],
            order: [[1, 'desc']],
          });
      }});
}
function calculation(selector){
    var gst_rate = $(selector + ' input[name=gst_bill]:checked').length;
    if(gst_rate > 0){
        $('.gst-rate-hide-show').show();
    }else{
        $('.gst-rate-hide-show').hide();
    }
    var bill_amount = 0;
    var total_charges = 0;
    var total_gst_value = 0
    $(selector+' .serviceslisttbl tbody tr').each(function(index, value) {
        var quantity = $(this).find('.quantity').val();
        var charges = $(this).find('.charges').val();
        var commission = $(this).find('.commission').val();
        var gst_percentage = $(this).find('.row-gst-rate').val();
        var gst_value = parseFloat(gst_percentage) / 100 * commission;
        total_gst_value += gst_value;

        $(this).find('.row-gst-amount').text(gst_value.toFixed(2));
        $(selector + ' .total-gst-rate').text('Rs. ' + total_gst_value.toFixed(2));
        
        var total = parseFloat(charges) + parseFloat(commission);
        if(gst_rate > 0){
            total += parseFloat(gst_value);
        }
        total = parseInt(quantity) * total;
        if(isNaN(total)){
            total = 0;
        }
        $(this).find('.row-total-amount').html('Rs. ' + total.toFixed(2));
        bill_amount += total;
        $(selector+' .bill-amount').val(bill_amount.toFixed(2));
        $(selector+' .total-bill-amount').html('Rs. ' + bill_amount.toFixed(2));
        if(charges != ""){
            total_charges += parseFloat(charges);
        }

        if(total_charges > 0){
            $(selector+' .charges-hide-show').show();
            $(selector+' .total-text').attr('colspan', 5);
        }else{
            $(selector+ ' .charges-hide-show').hide();
            $(selector+ ' .total-text').attr('colspan', 3);
        }

    });
}
function paymentStatusChangesEdit(){
    $('.credit-account').hide();
    $('.paid-amt-edit').hide();
    // $('.payment-responsible-by-edit').hide();
    $('#editForm select[name=account_id]').val('');
    var val = $('.payment_status-edit option:selected').val();
    if(val == "0"){
        // $('.payment-responsible-by-edit').show();
    }else if(val == "1"){
        $('.credit-account').show();
    }else if(val == "2"){
        // $('.payment-responsible-by-edit').show();
        $('.paid-amt-edit').show();
        $('.credit-account').show();
    }

    // if(val == 1){
    //     $('.paid-amt-edit').hide();
    //     $('.payment-responsible-by-edit').hide();
    // }else{
    //     $('.paid-amt-edit').show();
    //     $('.payment-responsible-by-edit').show();
    //     $('.pending-amount').val($('.bill-amount').val());
    // }

}

function paymentStatusChanges(){
    $('.credit-account').hide();
    $('.paid-amt').hide();
    $('.payment-responsible-by').hide();
    $('#add1 select[name=account_id]').val('');
    var val = $('.payment_status option:selected').val();
    if(val == "0"){
        $('#add1 select[name=account_id]').val($('#add1 select[name=account_id] option.pending').val());
        $('.payment-responsible-by').show();
    }else if(val == "1"){
        $('.credit-account').show();
    }else if(val == "2"){
        $('.credit-account').show();
        $('.paid-amt').show();
        $('.payment-responsible-by').show();
        // $('.pending-amount').val($('.bill-amount').val());
    }

}
$(document).ready(function(){
    $('#menu1').hide();
  });

function incomeTab(e){
   $('#home').hide();
   $('#menu1').hide();
   $($(e).attr('href')).show();
}