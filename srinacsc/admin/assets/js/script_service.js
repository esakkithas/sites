
function confirmEditService(){
    var charges = $('.charges').val();
    if(parseInt(charges) > 0){ 
        var fees_account_id = $('.fees_account_id option:selected').val();
        if(fees_account_id == undefined){
            alert('Please select debit account');
            return false;
        }
    }
    $('#edit form :submit').prop('disabled', true);
    var formData = $('#edit form').serialize();
    $.ajax({url: "services/edit", method:'POST', dataType: 'json',data:formData,success: function(result){
        getServicesList();
        $('#edit .msg').html('Updated Successfully');
        $('#edit form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}

function addService(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    $.ajax({url: "services/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        getServicesList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function viewService(id){  //console.log(servicesList[id])

    $('.modal-body .msg').text('');
    $('#view1').modal('show');
    $('#view1 .id').html(servicesList[id]['service_id']);
    $('#view1 .name').html(servicesList[id]['service_name']);
    $('#view1 .fees').html(servicesList[id]['fees']);
    $('#view1 .commission').html(servicesList[id]['commission']);
    $('#view1 .total').html(parseInt(servicesList[id]['fees']) + parseInt(servicesList[id]['commission']));
    $('#view1 .required_documents').html(servicesList[id]['required_documents']);
    $('#view1 .url').html('<a href="'+servicesList[id]['url']+'" target="_blank">'+servicesList[id]['url']+'</a>');
    $('#view1 .steps').html(servicesList[id]['steps']);

    if(servicesList[id]['category_slug'] == 'products'){
        $('.stock-tr').show();
    }else{
        $('.stock-tr').hide();
    }





        $.ajax({url: "services/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            $('#view1 .stock').html(result['stock']);
            // if(result['root_category_slug'] == 'services'){
            //     $('.view-charges').show();
            // }else{
            //     $('.view-charges').hide();
            // }

        // $('.modal-body .msg').text('');
        // $('#view1').modal('show');
        // $('#view1 .id').html(result['data']['id']);
        // $('#view1 .name').html(result['data']['name']);
        // $('#view1 .fees').html(result['data']['fees']);
        // $('#view1 .commission').html(result['data']['commission']);
        // $('#view1 .total').html(parseInt(result['data']['fees']) + parseInt(result['data']['commission']));
        // $('#view1 .required_documents').html(result['data']['required_documents']);
        // $('#view1 .url').html('<a href="'+result['data']['url']+'" target="_blank">'+result['data']['url']+'</a>');
        // $('#view1 .steps').html(result['data']['steps']);
    }});

}
function editService(id){
    
    $.ajax({url: "services/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);

        // if(result['root_category_slug'] == 'services'){
        //     $('.edit-charges').show();
        //     $('.edit-debit-account').show();
        // }else{
        //     $('.edit-charges').hide();
        //     $('.edit-debit-account').hide();
        // }

        $('#edit input[name=fees]').val(result['data']['fees']);
        $('#edit input[name=commission]').val(result['data']['commission']);
        $('#edit textarea[name=required_documents]').val(result['data']['required_documents']);
        $('#required_documents').summernote('destroy')
        $('#required_documents').summernote({
            height: 400,
        focus: true
          });
        $('#edit textarea[name=steps]').val(result['data']['steps']);
        $('#edit input[name=url]').val(result['data']['url']);

        $('#edit select[name=fees_account_id]').val(result['data']['fees_account_id']);

        $('#edit input[name=mrp]').val(result['data']['mrp']);
        $('#edit input[name=product_code]').val(result['data']['product_code']);
        $('#edit input[name=barcode]').val(result['data']['barcode']);
        $('#edit input[name=hsn]').val(result['data']['hsn']);
        $('#edit input[name=igst]').val(result['data']['igst']);
        $('#edit input[name=display_name]').val(result['data']['display_name']);


        // $('#edit #answerInput').val(result['data']['category_id']);
        datalist('answerInput', result['data']['category_id']);
        // $('#answerInput').trigger('input');
    }});
}
function deleteService(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "services/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            getServicesList();
           }});
      }
}
var servicesList = {};
function getServicesList(){
    $.ajax({url: "services/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['service_id']+'</td>';
            html += '<td>'+element['category_name']+'</td>';
            html += '<td>'+(element['product_code'] ? element['product_code'] : '')+'</td>';
            html += '<td>'+element['service_name']+'</td>';
            html += '<td>'+element['fees']+'</td>';
            html += '<td>'+element['commission']+'</td>';
            html += '<td> '+( parseFloat(element['fees']) + parseFloat(element['commission']) )+'</td>';
            servicesList[element['service_id']] = element;
            html += '<td><button type="button" class="btn btn-primary" onclick="viewService('+element['service_id']+'); return false">view</button> ';
           html += '<button type="button" class="btn btn-primary" onclick="editService('+element['service_id']+'); return false" '+(isAdmin!=1?'disabled':'')+'>Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deleteService('+element['service_id']+', \''+element['name']+'\'); return false" '+(isAdmin!=1?'disabled':'')+'>Delete</button></td>'
            html += '</tr>'
        });
        $('#services-tbl').DataTable().destroy();
        $('#services-tbl tbody').html(html);
        

            $('#services-tbl').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "bDestroy": true
              });

      }});
}


