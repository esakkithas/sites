function login(){
   
    var formdata = $("#loginform").serialize();
    $.ajax({url: "auth", method:'POST',data:formdata,dataType:'json',success: function(result){
        if(result.status == "success"){
            window.location = "../services";
        }else{
            alert("Invalid username/password");
        }
      
    }});
}
function removeMessage(){
    $('.modal-body .msg').text('');
}

function addRow(e, selector){
    var length = $(selector+' .serviceslisttbl tbody tr').length;
     var temp = $(e).closest('tr').clone();
     temp.find('.service-name').val('');
     temp.find('.quantity').val('');
     temp.find('.charges').val('');
     temp.find('.id-services-in-income').val('');
     temp.find('.fees_account_id').prop('selectedIndex',0);
     temp.find('.total-amount').val('');
     temp.find('.commission').val('');
     temp.find('.custom-select1').prop('selectedIndex',0);
     temp.find('.refno').val('');
     temp.find('.service-name').attr('id', 'service-name'+length);
     temp.find('.service-name-hidden').attr('id', 'service-name'+length+'-hidden');
     temp.appendTo(".modal.show #serviceslisttbl tbody");
     inputEvent(selector);
     calculation(selector);
    if($('.modal.show .serviceslisttbl tbody tr').length==1){
        $('.modal.show .serviceslisttbl .fa-trash').hide();
    }else{
        $('.modal.show .serviceslisttbl .fa-trash').show();
    }
}
function deleteRow(e, selector){
    $(e).closest('tr').remove();
    if($('.modal.show .serviceslisttbl tbody tr').length==1){
        $('.modal.show .serviceslisttbl .fa-trash').hide();
    }
    calculation(selector);
}

function inputEvent(selector){
    var inputlist = document.querySelectorAll('input[list]');
    for (var i = 0, len = inputlist.length; i < len; i++) {
        inputlist[i].addEventListener('input', function(e) { console.log('here 1');
            var input = e.target;
            // datalist(input.getAttribute('id'),input.value); 
                list = input.getAttribute('list'),
                options = document.querySelectorAll('#' + list + ' option'),
                hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden'),
                inputValue = input.value;
                // console.log(options);
            // hiddenInput.value = inputValue;
        
            for(var i = 0; i < options.length; i++) {
                var option = options[i]; 
                if(option.value.trim() == inputValue.trim()) { 
                    hiddenInput.value = option.getAttribute('data-value');
                    var r = $('#'+input.getAttribute('id') + '-hidden');
                    r.closest('tr').find('.commission').val(option.getAttribute('data-comm'));
                    r.closest('tr').find('.charges').val(option.getAttribute('data-charges'));
                    r.closest('tr').find('.fees_account_id').val(option.getAttribute('data-fees_account_id'));
                    r.closest('tr').find('.quantity').val(1);
                    var title = 'HSN: ' + option.getAttribute('data-hsn');
                    title += ', ' + option.getAttribute('data-igst') + ' %'
                    r.closest('tr').find('.row-gst-amount').attr('title', title);
                    r.closest('tr').find('.row-gst-rate').val(option.getAttribute('data-igst'));
                    calculation(selector);
                    break;
                }
            }
        });
      }
}



function datalist(id, inputValue){
    var input = document.getElementById(id);
    var list = input.getAttribute('list');
    var  options = document.querySelectorAll('#' + list + ' option');
    var hiddenInput = document.getElementById(id + '-hidden');
    // inputValue = input.value;

    // hiddenInput.value = inputValue;
// console.log(options);
    for(var i = 0; i < options.length; i++) {
        var option = options[i]; 
        console.log(option.getAttribute('data-value'));
        console.log(inputValue); 
        if(option.getAttribute('data-value').trim() == inputValue.trim()) { 
            console.log('test');
            console.log(option.getAttribute('data-value'));
            hiddenInput.value = option.getAttribute('data-value');
            input.value =  option.getAttribute('value');
            break;
        }
        console.log('..');
    }
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

$( document ).ready(function() {
    getServicesList();
    getCategoriesList();
    getcustomersList();
    getincomeList();
    getexpenseList();
    getemployeesList();
    getassetsList();
    inputEvent('#add1');
    paymentStatusChanges();
    $('body').bind('cut copy', function(event) {
        event.preventDefault();
    });
    $(".serviceslisttbl").each(function(){
        if($(this).find('tbody tr').length==1){
            $(this).find('.fa-trash').hide();
        }else{
            $(this).find('.fa-trash').show();
        }
      });
});



