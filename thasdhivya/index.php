<!DOCTYPE html>
<html>  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index, follow">
    <title>Boomerang 2 - MultiPurpose Template</title>

    <!-- Essential styles -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> 
    <link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen"> 
     
    <!-- Boomerang styles -->
        <link id="wpStylesheet" type="text/css" href="css/global-style.css" rel="stylesheet" media="screen">
        

    <!-- Favicon -->
    <link href="images/favicon.png" rel="icon" type="image/png">

    <!-- Assets -->
    <link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">    
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms-ie8.css">
    <![endif]-->

    <!-- Required JS -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui.min.js"></script>

    <!-- Page scripts -->
    	<link rel="stylesheet" href="assets/prettify/css/prettify.css">
<script src="assets/prettify/js/prettify.js"></script>


<!-- References: https://github.com/fancyapps/fancyBox -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<script type="text/javascript">	
	$(document).ready(function(){
		// Prettify code blocks
		prettyPrint();

		// Select all code on focus
		function SelectText(element) {
		    var doc = document
		        , text = doc.getElementById(element)
		        , range, selection
		    ;    
		    if (doc.body.createTextRange) {
		        range = document.body.createTextRange();
		        range.moveToElementText(text);
		        range.select();
		    } else if (window.getSelection) {
		        selection = window.getSelection();        
		        range = document.createRange();
		        range.selectNodeContents(text);
		        selection.removeAllRanges();
		        selection.addRange(range);
		    }
		}

		$(function() {
		    $('pre').click(function() {
		        var preID = $(this).attr('id');
		        SelectText(preID);
		    });
		});
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });

	});
</script>
<style>
    span.albums {
        padding: 5px 25px;
    }
    .row.photos{
        padding-bottom: 25px;
    }
    .navbar-wp1{
        background: #3E5C9A;
    }
    .navbar-wp .navbar-nav>li>a1{
        color: #fff;
    }
    .cuselText label {
        padding-left: 10px;
    }

    .btn-alt:hover, .btn-alt:focus, .btn-alt:active, .btn-alt.active, .open .dropdown-toggle.btn-alt, .btn-alt{
        background: #b00039;
        border-color: #b00039;
    }
    .navbar-wp .navbar-nav>.open>a, .navbar-wp .navbar-nav>.open>a:hover, .navbar-wp .navbar-nav>.open>a:focus {
        background: #b00039;
        border-color: #b00039;
    }
    ul.categories>li>a:hover {
        background: #b00039;
    }
    .navbar-wp .navbar-nav>li>a:hover, .navbar-wp .navbar-nav>li>a:focus{
        background: #b00039;
        border-color: #b00039;
    }
 
</style>
</head>
<body>
    
<!-- MODALS -->

<!-- MOBILE MENU - Option 2 -->
<section id="navMobile" class="aside-menu left">
    <form class="form-horizontal form-search">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button id="btnHideMobileNav" class="btn btn-close" type="button" title="Hide sidebar"><i class="fa fa-times"></i></button>
            </span>
        </div>
    </form>
    <div id="dl-menu" class="dl-menuwrapper">
        <ul class="dl-menu"></ul>
    </div>
</section> 

<!-- SLIDEBAR -->
<section id="asideMenu" class="aside-menu right">
    <form class="form-horizontal form-search">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Search..." />
            <span class="input-group-btn">
                <button id="btnHideAsideMenu" class="btn btn-close" type="button" title="Hide sidebar"><i class="fa fa-times"></i></button>
            </span>
        </div>
    </form>
    
    <h5 class="side-section-title">Optional sidebar menu</h5>
    <div class="nav">
        <ul>
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">About us</a>
            </li>
            <li>
                <a href="#">Blog</a>
            </li>
            <li>
                <a href="#">Work</a>
            </li>
            <li>
                <a href="#">Online shop</a>
            </li>
        </ul>
    </div>
    
    <h5 class="side-section-title">Social media</h5>
    <div class="social-media">
        <a href="#"><i class="fa fa-facebook facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus google"></i></a>
        <a href="#"><i class="fa fa-twitter twitter"></i></a>
    </div>
    
    <h5 class="side-section-title">Contact information</h5>
    <div class="contact-info">
        <h5>Address</h5>
        <p>5th Avenue, New York - United States</p>
        
        <h5>Email</h5>
        <p>hello@webpixels.ro</p>
        
        <h5>Phone</h5>
        <p>+10 724 1234 567</p>
    </div>
</section>

<!-- MAIN WRAPPER -->
<div class="body-wrap">

            <!-- HEADER -->
        <div id="divHeaderWrapper">
            <header class="header-standard-2">     
    <!-- MAIN NAV -->
    <div class="navbar navbar-wp navbar-arrow mega-nav" role="navigation">
        <div class="container">
            <div class="navbar-header">
        
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars icon-custom"></i>
                </button>

                <a class="navbar-brand" href="index.html" title="Boomerang | One template. Infinite solutions">
                    <img src="images/boomerang-logo-black-family2.png" alt="Boomerang | One template. Infinite solutions">
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-md hidden-lg">
                        <div class="bg-light-gray">
                            <form class="form-horizontal form-light p-15" role="form">
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control" placeholder="I want to find ...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-white" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="dropdown dropdown-meganav mega-dropdown-fluid">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home</a>
                 
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
                 
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery</a>
                 
                    </li>
            
          
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Family</a>
                    
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Profile</a>
                      
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
                      
                    </li>
                    <li class="dropdown-aux">
                        <a href="#" id="cmdAsideMenu" class="dropdown-toggle dropdown-form-toggle" title="Open slidebar">
                            <i class="fa fa-outdent"></i>
                        </a>
                    </li>
                </ul>
               
            </div><!--/.nav-collapse -->
        </div>
    </div>
</header>        </div>

<section class="slice bg-white hidden-sm1 hidden-xs1">
    <div class="container">
        <div class="wp-section relative">
            <form class="form-inline " role="form">
                <div class="inline-form-filters " style="background-color:#f5f5f5">
                  

                    <!-- Main filters -->
                    <div class="row ">
                        <div class="col-md-7 col-sm-8 col-xs-8">
                            <div class="form-group form-group-lg">
                                <label class="sr-only">Search text</label>
                                <input type="text" id="searchText" class="form-control input-lg" placeholder="Search " value="">
                            </div>
                        </div>
                        <!-- <div class="col-md-2">
                         <div class="form-group form-group-lg field_select">
                                <label class="sr-only" >Category</label>
                                <select class="form-control select_styled  no-padding" >
                                    <option value="1"> </option>
                                </select>
                            </div>
                  
                        </div> -->
                        <div class="col-md-2 hidden-sm hidden-xs">
                         <div class="form-group form-group-lg field_select">
                                <label class="sr-only">Area</label>
                                <div class="cusel form-control select_styled  no-padding  cuselScrollArrows" id="cuselFrame-cuSel-1687542516940-0" style="width:158px" tabindex="0"><div class="cuselFrameRight"></div><div data-class="" class="cuselText "><label>All Photos</label></div><div class="cusel-scroll-wrap" style="display: none; visibility: visible;"><div class="cusel-scroll-pane" id="cusel-scroll-cuSel-1687542516940-0">
                                    <span val="1" class="cuselItem cuselActive"><label> All Photos</label></span>
                                </div></div><input type="hidden" id="cuSel-1687542516940-0" name="" value="1"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <a href="#" class="btn btn-lg btn-block btn-alt" onclick=" return false">
                                <span> &nbsp; </span><i class="fa fa-search"></i>
                            </a>
                        </div>
                        
                    </div>

                </div>
             </form>
        </div>
    </div>
</section>

    <section class="slice bg-white">
        <div class="wp-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget">
						    <ul class="categories">

<?php 
$parent_folders = scandir('data');
foreach($parent_folders as $parent_folder) {
    if (is_dir("data/".$parent_folder) && $parent_folder != '.' && $parent_folder != '..') {
        echo '<li><a tabindex="-1" href="#">'.$parent_folder.'</a>';
        echo '<ul>';
        $sub_folders = scandir('data/'.$parent_folder);
        foreach($sub_folders as $sub_folder){
            if (is_dir('data/'.$parent_folder.'/'.$sub_folder) && $sub_folder != '.' && $sub_folder != '..'){
                echo '<li><span class="albums"><input type="checkbox"> '.$sub_folder.' </span></li>';
            }
        }
        echo '</ul>';
        echo '</li>';
    }
}
?>

                                <li><a tabindex="-1" href="#">Callouts</a></li>
    <li><a tabindex="-1" href="#">Bengaluru</a>
        <ul>
            <li><span class="albums"><input type="checkbox"> Place 1 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
        </ul>
    </li>
    <li><a tabindex="-1" href="#">Callouts</a></li>
    <li><a tabindex="-1" href="#">Chennai</a>
        <ul>
            <li><span class="albums"><input type="checkbox"> Place 1 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
            <li><span class="albums"><input type="checkbox"> Place 2 </span></li>
        </ul>
    </li>
    <li><a tabindex="-1" href="#">Callouts</a></li>
</ul>						</div>                    
					</div>
                    <div class="col-md-9">
                    	<!-- Code -->
                    	<div class="wp-example">
  
                    		<div class="section-title-wr">
                        		<h3 class="section-title left"><span>Bengaluru -> Ulsoor Lake</span></h3>
                                <div class="filter sort-filter pull-right">
                                    <label>Sort by</label>
                                    <select>
                                        <option>Date </option>
                                        <option>Rating</option>
                                    </select>
                                </div>
                            </div>





<div class="container">
	<div class="row">
		<div class='list-group gallery'>
            <div class='col-sm-12 col-xs-12 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-12 col-xs-12 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-12 col-xs-12 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-12 col-xs-12 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-12 col-xs-12 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-12 col-xs-12 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
        </div> <!-- list-group / end -->
	</div> <!-- row / end -->
</div> 



                            <div class="row photos">
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-3.jpg" class="img-responsive" alt=""><a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-4.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-5.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            </div>

                            <div class="row photos">
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-3.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-4.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-5.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            </div>

                            <div class="row photos">
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-3.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-4.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-5.jpg" class="img-responsive" alt="">
                                        <a href="#" class="badge-corner">
	                            			<span class="fa fa-heart"></span>
	                            		</a>
	                            	</div>
                            	</div>
                            </div>
                            <div class="section-title-wr">
                        		<h3 class="section-title left"><span>Bengaluru -> Ulsoor Lake</span></h3>
                            </div>
                            <div class="row photos">
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-3.jpg" class="img-responsive" alt="">
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-4.jpg" class="img-responsive" alt="">
	                            	</div>
                            	</div>
                            	<div class="col-md-4">
                            		<div class="relative">
	                            		<img src="images/prv/wk-big-img-5.jpg" class="img-responsive" alt="">
	                            	</div>
                            	</div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">
        
            

            
            <div class="row">
                <div class="col-lg-9 copyright">
                Copyright ©<?php echo date('Y'); ?>  Web Pixels. All rights reserved.
                   
                </div>
                <div class="col-lg-3">
                    <a href="#" title="Made with love by Web Pixels" target="_blank" class="">
                    <img src="images/webpixels-footer-logo.png" alt="Web Pixels - Designing Forward | Logo" class="pull-right">
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>

<!-- Essentials -->
<script src="js/modernizr.custom.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script src="js/jquery.easing.js"></script>
<script src="js/jquery.metadata.js"></script>
<script src="js/jquery.hoverup.js"></script>
<script src="js/jquery.hoverdir.js"></script>
<script src="js/jquery.stellar.js"></script>

<!-- Boomerang mobile nav - Optional  -->
<script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script>
<script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script>

<!-- Forms -->
<script src="assets/ui-kit/js/jquery.powerful-placeholder.min.js"></script> 
<script src="assets/ui-kit/js/cusel.min.js"></script>
<script src="assets/sky-forms/js/jquery.form.min.js"></script>
<script src="assets/sky-forms/js/jquery.validate.min.js"></script>
<script src="assets/sky-forms/js/jquery.maskedinput.min.js"></script>
<script src="assets/sky-forms/js/jquery.modal.js"></script>

<!-- Assets -->
<script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="assets/page-scroller/jquery.ui.totop.min.js"></script>
<script src="assets/mixitup/jquery.mixitup.js"></script>
<script src="assets/mixitup/jquery.mixitup.init.js"></script>
<script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="assets/waypoints/waypoints.min.js"></script>
<script src="assets/milestone-counter/jquery.countTo.js"></script>
<script src="assets/easy-pie-chart/js/jquery.easypiechart.js"></script>
<script src="assets/social-buttons/js/rrssb.min.js"></script>
<script src="assets/nouislider/js/jquery.nouislider.min.js"></script>
<script src="assets/owl-carousel/owl.carousel.js"></script>
<script src="assets/bootstrap/js/tooltip.js"></script>
<script src="assets/bootstrap/js/popover.js"></script>

<!-- Sripts for individual pages, depending on what plug-ins are used -->

<!-- Boomerang App JS -->
<script src="js/wp.app.js"></script>
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Temp -- You can remove this once you started to work on your project -->
<script src="js/jquery.cookie.js"></script>
<script src="js/wp.switcher.js"></script>
<script type="text/javascript" src="js/wp.ga.js"></script>


</body>
</html>